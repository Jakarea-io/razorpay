<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('documents', function (Blueprint $table) {
            $table->increments('document_id');
            $table->string('student_application_id');
            $table->string('transfer_certificate');
            $table->string('conduct_certificate');
            $table->string('mark_sheet');
            $table->string('provisional_degree_certificate');
            $table->string('photo');
            $table->string('aadhar_card_front');
            $table->string('aadhar_card_back');
            $table->string('parent_aadhar_card_front');
            $table->string('parent_aadhar_card_back');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
