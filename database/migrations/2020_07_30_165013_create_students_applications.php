<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsApplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_applications', function (Blueprint $table) {
            //$table->id();
            $table->increments('student_application_id');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('gender');
            $table->string('date_of_birth');
            $table->string('email_id');
            $table->string('alt_email_id')->nullable();
            $table->string('mobile_number');
            $table->string('alt_mobile_number')->nullable();
            $table->string('student_aadhar_number')->nullable();
            $table->string('parent_aadhar_number')->nullable();
            $table->string('nationality')->default('Indian');
            $table->string('blood_group')->nullable();
            $table->string('religion')->nullable();
            $table->string('community')->nullable();

            $table->string('address');
            $table->string('country')->default('India');
            $table->string('state');
            $table->string('city');
            $table->string('pin_code');

            $table->string('father_name');	
            $table->string('father_occupation');	
            $table->string('father_mobile_number')->nullable();
            $table->string('mother_name');
            $table->string('mother_occupation')->nullable();
            $table->string('mother_mobile_number')->nullable();
            $table->string('guardian_name')->nullable();	
            $table->string('guardian_occupation')->nullable();		
            $table->string('guardian_mobile_number')->nullable();	

            $table->string('th_board');
            $table->string('th_other_board')->nullable();
            $table->string('th_name_of_school');
            $table->string('th_percentage_cgpa');
            $table->string('th_passing_month');
            $table->string('th_passing_year');
            $table->string('th_medium_of_instruction');
            $table->string('th_passed_in_single_attempt');

            $table->string('tlv_board');
            $table->string('tlv_other_board')->nullable();
            $table->string('tlv_name_of_school');
            $table->string('tlv_percentage_cgpa');
            $table->string('tlv_passing_month');
            $table->string('tlv_passing_year');
            $table->string('tlv_medium_of_instruction');
            $table->string('tlv_passed_in_single_attempt');

            $table->string('degree');
            $table->string('specialization')->nullable();
            $table->string('dept_branch_subject')->nullable();
            $table->string('name_of_college');
            $table->string('name_of_university');
            $table->string('overall_cgp');
            $table->string('degree_passing_month')->nullable();
            $table->string('degree_passing_year')->nullable();
            $table->string('degree_medium_of_instruction');
            $table->string('other_educational_information')->nullable();
            $table->string('ug_time_of_applying')->nullable();

            // $table->string('select_test')->nullable();
            // $table->string('date_of_test')->nullable();
            // $table->string('registration_number')->nullable();
            // $table->string('overall_score')->nullable();
            // $table->string('overall_percentile')->nullable();
            
            $table->string('program');
            $table->string('specific_programme');

            $table->string('razorpay_payment_id')->nullable();
            $table->string('razorpay_order_id')->nullable();
            $table->string('razorpay_signature')->nullable();
            $table->string('application_status')->nullable();
            $table->string('seen')->nullable();
     
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_applications');
    }
}
