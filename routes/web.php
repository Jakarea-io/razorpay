<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Auth::routes();
Route::get('/logout', function(){
    Auth::logout();
    return redirect('/online-application');
});

Route::get('/register', function(){
    Auth::logout();
    return redirect('/online-application');
});


// Route::get('/', function(){
//     return 'Coming soon!';
// });

Route::get('/', 'StudentApplicationController@create');
Route::get('/online-application', 'StudentApplicationController@create');
Route::post('/online-application', 'StudentApplicationController@store');


Route::get('/upload-documents',  'DocumentUploadController@create');
Route::post('/upload-documents', 'DocumentUploadController@store');
Route::get('/tuition-fee', 'DocumentUploadController@tuitionFee');

// Route::get('/applications', 'StudentApplicationController@index')->middleware(['auth']);
// Route::get('/applications/all', 'AdminController@all')->middleware(['auth']);

Route::get('/applications', 'AdminController@all')->middleware(['auth']);
Route::get('/applications/download', 'AdminController@downloadExcel')->middleware(['auth']);

Route::get('/applications/{id}', 'StudentApplicationController@show')->where('id', '[0-9]+')->middleware(['auth']);
Route::get('/applications/{id}/pdf', 'StudentApplicationController@showPdf')->where('id', '[0-9]+')->middleware(['auth']);

Route::get('/applications/filter', 'AdminController@all')->middleware(['auth']);

Route::get('/applications/{id}/accept', 'SmsController@acceptApplication')->where('id', '[0-9]+')->middleware(['auth']);
Route::get('/applications/{id}/reject', 'SmsController@rejectApplication')->where('id', '[0-9]+')->middleware(['auth']);
Route::get('/applications/{id}/verified', 'SmsController@feeVerified')->where('id', '[0-9]+')->middleware(['auth']);
Route::get('/applications/{id}/documents', 'AdminController@showDocuments')->where('id', '[0-9]+')->middleware(['auth']);
Route::get('/applications/{id}/documents/accept', 'SmsController@acceptDocuments')->where('id', '[0-9]+')->middleware(['auth']);

Route::get('/application-status', 'StudentApplicationController@status');

Route::get('/students-applications/:id', 'StudentApplicationController@show')->where('id', '[0-9]+');
Route::get('/payment', 'PaymentController@payment');
Route::post('/payment-complete', 'PaymentController@complete');
Route::post('/pay-now', 'PaymentController@payNow');
Route::post('/pay-process', 'PaymentController@payNowProcess');
Route::get('/payment-success', 'PaymentController@success');

Route::get('/home', function () {
    return redirect('/applications');
});