<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $primaryKey = 'payment_id';
    protected $table = 'payments';

    protected $fillable = [
        'student_application_id',
        'razorpay_order_id',
        'razorpay_signature',
        'razorpay_payment_id',
        'amount',
        'type'
    ];
}
