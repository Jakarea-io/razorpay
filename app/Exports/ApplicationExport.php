<?php

namespace App\Exports;

use App\StudentApplication;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;

class ApplicationExport implements FromCollection,FromQuery
{
    use Exportable;

    public function __construct(int $year)
    {
        $this->year = $year;
    }

    public function query()
    {
        return StudentApplication::query()->whereYear('created_at', $this->year);
    }
}
