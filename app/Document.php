<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $primaryKey = 'document_id';
    protected $table = 'documents';

    protected $fillable = [
        'student_application_id'
        ,'transfer_certificate'
        ,'conduct_certificate'
        ,'mark_sheet'
        ,'provisional_degree_certificate'
        ,'date_of_birth'
        ,'aadhar_card_front'
        ,'aadhar_card_back'
        ,'parent_aadhar_card_front'
        ,'parent_aadhar_card_back'
    ];
}
