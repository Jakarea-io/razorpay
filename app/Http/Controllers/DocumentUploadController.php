<?php

namespace App\Http\Controllers;

use App\StudentApplication;
use App\Document;
use Storage;
use Illuminate\Http\Request;
use Mail;
use App\Mail\DocumentUpload;
use App\Mail\StudentApplicationMail;
use App\Payment;
use Razorpay\Api\Api;
use Illuminate\support\Str;
use Validator;

class DocumentUploadController extends Controller
{

    //private $rozarpayId = "rzp_test_jNmnOqmpjFtjG2";
    //private $rozarpayKey = "Urtcj606HVhrvwbup3fv1bkQ";
    
    private $rozarpayId = "rzp_live_8xpMuIBTxHKto1";
    private $rozarpayKey = "I6XtOLWdZWXDqY9rikcy35XX";

    public function create(Request $request){
        $mobile_number = $request->mobile_number?$request->mobile_number : null;
        $date_of_birth = $request->date_of_birth?$request->date_of_birth : null; 

        $application = StudentApplication::where('mobile_number', $request->mobile_number)
                            ->where('date_of_birth', $request->date_of_birth)->first();
        $hasDocuments = Document::where('student_application_id', $application->student_application_id)->first();
        if($application == null){
            return redirect('application-status');
        }
        return view('documents.upload',compact('application','hasDocuments'));

    }

    public function tuitionFee(Request $request){
        $mobile_number = $request->mobile_number?$request->mobile_number : null;
        $date_of_birth = $request->date_of_birth?$request->date_of_birth : null; 

        $application = StudentApplication::where('mobile_number', $request->mobile_number)
                            ->where('date_of_birth', $request->date_of_birth)->first();
        $hasDocuments = Document::where('student_application_id', $application->student_application_id)->first();
        if($application == null){
            return redirect('application-status');
        }
        return view('documents.payment',compact('application','hasDocuments'));

    }

    public function store(Request $request){
        $student_application_id = $request->student_application_id;
        $StudentApplication = StudentApplication::where('student_application_id', $student_application_id)->first();
        if(empty($StudentApplication)){
            return redirect()->back()->with('error', 'Something went wrong! Please try again.');
        }
        //$min = $StudentApplication->fee_remaining <= 10000 ? $StudentApplication->fee_remaining : 5000;
        $path = 'public/documents/'.$student_application_id;
        
        $this->validate($request, [
            'photo' => 'required|mimes:jpg,jpeg,pdf,png|max:2048',
            'transfer_certificate' => 'mimes:jpg,jpeg,pdf,png|max:2048',
            'conduct_certificate' => 'mimes:jpg,jpeg,pdf,png|max:2048',
            'mark_sheet' => 'required|mimes:jpg,jpeg,pdf,png|max:2048',
            'provisional_degree_certificate' => 'mimes:jpg,jpeg,pdf,png|max:2048',
             
            'aadhar_card_front' => 'mimes:jpg,jpeg,pdf,png|max:2048',
            'aadhar_card_back' => 'mimes:jpg,jpeg,pdf,png|max:2048',
            'parent_aadhar_card_front' => 'mimes:jpg,jpeg,pdf,png|max:2048',
            'parent_aadhar_card_back' => 'mimes:jpg,jpeg,pdf,png|max:2048',
            //'amount' => 'required|numeric|min:'.$min
        ]);

        $Document = new Document;
        $Document->student_application_id = $request->student_application_id;

        if ($request->hasFile('transfer_certificate')) {
            $Document->transfer_certificate = time().'_'.mt_rand().'.'.substr($request->file('transfer_certificate')->extension(),-20);
            $request->transfer_certificate->storeAs($path,$Document->transfer_certificate);
        }

        if ($request->hasFile('photo')) {
            $Document->photo = time().'_'.mt_rand().'.'.substr($request->file('photo')->extension(),-20);
            $request->photo->storeAs($path,$Document->photo);
        }

        if ($request->hasFile('conduct_certificate')) {
            $Document->conduct_certificate = time().'_'.mt_rand().'.'.substr($request->file('conduct_certificate')->extension(),-20);
            $request->conduct_certificate->storeAs($path,$Document->conduct_certificate);
        }

        if ($request->hasFile('mark_sheet')) {
            $Document->mark_sheet = time().'_'.mt_rand().'.'.substr($request->file('mark_sheet')->extension(),-20);
            $request->mark_sheet->storeAs($path,$Document->mark_sheet);
        }
        if ($request->hasFile('provisional_degree_certificate')) {
            $Document->provisional_degree_certificate = time().'_'.mt_rand().'.'.substr($request->file('provisional_degree_certificate')->extension(),-20);
            $request->provisional_degree_certificate->storeAs($path,$Document->provisional_degree_certificate);
        }
        
        if ($request->hasFile('aadhar_card_front')) {
            $Document->aadhar_card_front = time().'_'.mt_rand().'.'.substr($request->file('aadhar_card_front')->extension(),-20);
            $request->aadhar_card_front->storeAs($path,$Document->aadhar_card_front);
        }
        if ($request->hasFile('aadhar_card_back')) {
            $Document->aadhar_card_back = time().'_'.mt_rand().'.'.substr($request->file('aadhar_card_back')->extension(),-20);
            $request->aadhar_card_back->storeAs($path,$Document->aadhar_card_back);
        }
        if ($request->hasFile('parent_aadhar_card_front')) {
            $Document->parent_aadhar_card_front = time().'_'.mt_rand().'.'.substr($request->file('parent_aadhar_card_front')->extension(),-20);
            $request->parent_aadhar_card_front->storeAs($path,$Document->parent_aadhar_card_front);
        }
        if ($request->hasFile('parent_aadhar_card_back')) {
            $Document->parent_aadhar_card_back = time().'_'.mt_rand().'.'.substr($request->file('parent_aadhar_card_back')->extension(),-20);
            $request->parent_aadhar_card_back->storeAs($path,$Document->parent_aadhar_card_back);
        }

        if($Document->save()){
            $StudentApplication->application_status = 'Verify pending';
            $StudentApplication->save();

            $link = url('application-status?mobile_number='.$StudentApplication->mobile_number.'&date_of_birth='.$StudentApplication->date_of_birth);
            $details = [
                "subject" => "Document Upload Success",
                "name" => $StudentApplication->first_name ." ". $StudentApplication->last_name,
                "text" => "Your document upload success, Please wait for document verification. You can also check application status by visiting $link",
                "link" => $link,
                "sms" => "Your document upload success, Please wait for document verification. You can also check application status by visiting https://bit.ly/application-status"
            ];

            Mail::to($StudentApplication->email_id)->send(new StudentApplicationMail($details));
    
            $url = 'http://pay4sms.in';
            $token = 'ad7bf8c43708efbb5f80081ee0104206';
            $credit = '2';
            $sender = 'PAYMSG';
            $message = $details['name'] .' '. $details['sms'];
            $number = $StudentApplication->mobile_number;
            $sendsms = new sendsms($url,$token);
            $message_id = $sendsms->sendmessage($credit,$sender,$message,$number);
            $dlr_status = $sendsms->checkdlr($message_id);
            $available_credit = $sendsms->availablecredit($credit);

            return redirect($link)->with('success','Your document upload success, Please wait for document verification.');
        }else{
            return redirect()->back()->with('warning','Something went wrong! Please try again.');
        }
    }
}