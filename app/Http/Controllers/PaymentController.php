<?php

namespace App\Http\Controllers;
use App\StudentApplication;
use App\Payment;
use App\Document;
use Illuminate\Http\Request;
use Razorpay\Api\Api;
use Illuminate\support\Str;
use Mail;
use App\Mail\StudentApplicationMail;
use DB;


class PaymentController extends Controller
{
 private $rozarpayId = "rzp_live_8xpMuIBTxHKto1";
 private $rozarpayKey = "I6XtOLWdZWXDqY9rikcy35XX";
   
    //private $rozarpayId = "rzp_test_jNmnOqmpjFtjG2";
    //private $rozarpayKey = "Urtcj606HVhrvwbup3fv1bkQ";
    
    public function payNow(Request $request)
    {
        
        $student_application_id = $request->student_application_id;
        $StudentApplication = StudentApplication::where('student_application_id', $student_application_id)->first();
        $min = $StudentApplication->fee_remaining <= 10000 ? $StudentApplication->fee_remaining : 5000;
        
        $this->validate($request, [
            'amount' => 'required|numeric|min:'.$min
        ]);

        $api = new Api($this->rozarpayId, $this->rozarpayKey);
            $receiptId = Str::random(16);
            $order = $api->order->create(array(
                'receipt' => $receiptId,
                'amount' => $request->amount * 100,
                'currency' => 'INR',
                'payment_capture' => 1
                )
            );

            $response = [
                'orderId' => $order['id'],
                'rozarpayId' => $this->rozarpayId,
                'amount' => $request->amount * 100,
                'name' =>  $StudentApplication->first_name .' ' . $StudentApplication->last_name,
                'currency' => 'INR',
                'email' => $StudentApplication->email_id,
                'contactNumber' => $StudentApplication->mobile_number,
                'dob' => $StudentApplication->date_of_birth,
                'address' => $StudentApplication->address,
                'Description' => 'Test rozarpay'
            ];
            $Payment = new Payment;
            $Payment->razorpay_order_id = $order['id'];
            $Payment->amount = $request->amount;
            $Payment->student_application_id = $request->student_application_id;
            //$Payment->save();
            $request->session()->put('Payment', serialize($Payment));
            return view('payment.paynow',compact('response'));
    }

    public function payNowProcess(Request $request)
    {
        $signatureStatus = $this->signatureVerify(
            $request->razorpay_signature,
            $request->razorpay_payment_id,
            $request->razorpay_order_id
        );

        if($signatureStatus == true){
            $Payment = unserialize($request->session()->get('Payment'));
            $Payment->razorpay_signature = $request->razorpay_signature;
            $Payment->razorpay_payment_id = $request->razorpay_payment_id;
            if($Payment->save()){
                $StudentApplication = StudentApplication::where('student_application_id', $Payment->student_application_id)->first();
                $StudentApplication->fee_remaining = $StudentApplication->fee_remaining - $Payment->amount;
                $StudentApplication->amount = $StudentApplication->amount + $Payment->amount;
                $StudentApplication->application_status = 'Fee paid';
                $StudentApplication->razorpay_payment_id = $request->razorpay_payment_id;
                $StudentApplication->save();
                $link = url('application-status?mobile_number='.$StudentApplication->mobile_number.'&date_of_birth='.$StudentApplication->date_of_birth);
                $details = [
                    "subject" => "Tuition Fee Payment Success",
                    "name" => $StudentApplication->first_name ." ". $StudentApplication->last_name,
                    "text" => "Your tuition tee payment success, Please wait for verification.
                    You can also  check application status by visiting $link",
                    "link" => $link,
                    "sms" => "Your tuition fee payment success, Please wait for verification.
                    You can also check application status by visiting https://bit.ly/application-status"
                ];
    
                Mail::to($StudentApplication->email_id)->send(new StudentApplicationMail($details));
                
                $url = 'http://pay4sms.in';
                $token = 'ad7bf8c43708efbb5f80081ee0104206';
                $credit = '2';
                $sender = 'PAYMSG';
                $message = $details['name'] .' '. $details['sms'];
                $number = $StudentApplication->mobile_number;
                $sendsms = new sendsms($url,$token);
                $message_id = $sendsms->sendmessage($credit,$sender,$message,$number);
                $dlr_status = $sendsms->checkdlr($message_id);
                $available_credit = $sendsms->availablecredit($credit);
    
                return redirect('payment-success')->with('details', $details);

            }
        }

        return redirect('/');
    }

    public function documentPayment(Request $request)
    {
        
        $signatureStatus = $this->signatureVerify(
            $request->razorpay_signature,
            $request->razorpay_payment_id,
            $request->razorpay_order_id
        );
        if($signatureStatus == true){
            $Payment = unserialize($request->session()->get('Payment'));
           
            //$payment = Payment::where('razorpay_order_id', $request->razorpay_order_id)->first();
            $Payment->razorpay_signature = $request->razorpay_signature;
            $Payment->razorpay_payment_id = $request->razorpay_payment_id;
            if($Payment->save()){

                $StudentApplication = StudentApplication::where('student_application_id', $Payment->student_application_id)->first();
                $StudentApplication->fee_remaining = $StudentApplication->fee_remaining - $Payment->amount;
                $StudentApplication->amount = $StudentApplication->amount + $Payment->amount;
                $StudentApplication->application_status = 'Verify pending';
                $StudentApplication->razorpay_payment_id = $request->razorpay_payment_id;
                $StudentApplication->save();
                $link = url('application-status?mobile_number='.$StudentApplication->mobile_number.'&date_of_birth='.$StudentApplication->date_of_birth);
                $details = [
                    "subject" => "Document Upload and Payment Success",
                    "name" => $StudentApplication->first_name ." ". $StudentApplication->last_name,
                    "text" => "Your payment and document upload success, Please wait for document verification.",
                    "link" => $link,
                    "sms" => "Your payment and document upload success, Please wait for document verification."
                ];
    
                Mail::to($StudentApplication->email_id)->send(new StudentApplicationMail($details));
                $Document = unserialize($request->session()->get('Document'));
                $Document->save();

                $url = 'http://pay4sms.in';
                $token = 'ad7bf8c43708efbb5f80081ee0104206';
                $credit = '2';
                $sender = 'PAYMSG';
                $message = $details['name'] .' '. $details['sms'];
                $number = $StudentApplication->mobile_number;
                $sendsms = new sendsms($url,$token);
                $message_id = $sendsms->sendmessage($credit,$sender,$message,$number);
                $dlr_status = $sendsms->checkdlr($message_id);
                $available_credit = $sendsms->availablecredit($credit);
    
                return redirect('payment-success')->with('details', $details);

            }
        }
        else{
            Payment::where('razorpay_order_id', $request->razorpay_order_id)
                ->delete();
            return view('welcome');
        }

        return view('application.payment',compact('response'));
    }

    public function complete(Request $request)
    {
        
        $signatureStatus = $this->signatureVerify(
            $request->razorpay_signature,
            $request->razorpay_payment_id,
            $request->razorpay_order_id
        );

        if($signatureStatus == true){
            //$StudentApplication = StudentApplication::where('razorpay_order_id', $request->razorpay_order_id)->first();
            //$StudentApplication = new StudentApplication;
            $StudentApplication = unserialize($request->session()->get('StudentApplication'));
           
            $StudentApplication->razorpay_signature = $request->razorpay_signature;
            $StudentApplication->razorpay_payment_id = $request->razorpay_payment_id;
            if($StudentApplication->save()){
                $payment = new Payment;
                $payment->student_application_id = $StudentApplication->student_application_id;
                $payment->razorpay_signature = $StudentApplication->razorpay_signature;
                $payment->razorpay_payment_id = $StudentApplication->razorpay_payment_id;
                $payment->razorpay_order_id  = $StudentApplication->razorpay_order_id;
                $payment->amount = $StudentApplication->amount;
                $payment->save();

                $link = url('application-status?mobile_number='.$StudentApplication->mobile_number.'&date_of_birth='.$StudentApplication->date_of_birth);
                $details = [
                    "subject" => "Payment Success",
                    "name" => $StudentApplication->first_name ." ". $StudentApplication->last_name,
                    "link" => $link,
                    "text" => "Thanks for applying to JJ College. Your application for ".$StudentApplication->program." - ".$StudentApplication->specific_programme." is successfully submitted. We will update you in 24 hours regarding your application status.
                    You can also check application status by visiting " . $link,
                    "sms" => "Thanks for applying to JJ College. Your application for ".$StudentApplication->program." - ".$StudentApplication->specific_programme." is successfully submitted. We will update you in 24 hours regarding your application status.
                    You can also check application status by visiting https://bit.ly/application-status",
                ];
    
                Mail::to($StudentApplication->email_id)->send(new StudentApplicationMail($details));
            }
        
            $url = 'http://pay4sms.in';
            $token = 'ad7bf8c43708efbb5f80081ee0104206';
            $credit = '2';
            $sender = 'PAYMSG';
            $message = $details['name'] .' '. $details['sms'];
            $number = $StudentApplication->mobile_number;
            $sendsms = new sendsms($url,$token);
            $message_id = $sendsms->sendmessage($credit,$sender,$message,$number);
            $dlr_status = $sendsms->checkdlr($message_id);
            $available_credit = $sendsms->availablecredit($credit);

            return redirect('payment-success')->with('details', $details);
        }
        else{
            StudentApplication::where('razorpay_order_id', $request->razorpay_order_id)
                ->delete();
            return view('welcome');
        }

        return view('application.payment',compact('response'));
    }


    private function signatureVerify( $razorpay_signature,$razorpay_payment_id,$razorpay_order_id){
        
        try{
            $api = new Api($this->rozarpayId, $this->rozarpayKey);
            $attributes = array(
                'razorpay_signature' => $razorpay_signature,
                'razorpay_payment_id' => $razorpay_payment_id,
                'razorpay_order_id' => $razorpay_order_id
            );
            $order = $api->utility->verifyPaymentSignature($attributes);
            return true;
        }
        catch(\Exception $e){
            return false;
        }
    }

    public function success(){
        return view('payment.success');
    }
}


class sendsms
{
	private $url;
  	private $token;
	private $credit;
	private $message;
	private $number;
	private $sender;
	private $msgid; 
	public function __construct($url,$token)
	{
		$this->url = $url.'/';
		$this->token = '?token='.$token;
		$this->credit = '&credit=';
		$this->sender = '&sender=';
		$this->number = '&number=';
		$this->message = '&message=';
		$this->msgid = '&msgid=';
	}	
	public function __destruct()
	{
	}
	function sendme($smsurl)
	{
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_URL,$smsurl);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_HEADER,false);
		$result = curl_exec($curl);
        curl_close($curl);
		return $result;
	}
	public function sendmessage($credit,$sender,$message,$number)
	{
		$message = urlencode($message);
		$smsurl = $this->url.'sendsms/'.$this->token.$this->sender.$sender.$this->number.$number.$this->credit.$credit.$this->message.$message;
       
        $result = $this->sendme($smsurl);
		return $result;
	}
	public function checkdlr($message_id)
	{
		$smsurl = $this->url.'Dlrcheck/'.$this->token.$this->msgid.$message_id;
		$result = $this->sendme($smsurl);
		return $result;
	}
	public function availablecredit($credit)
	{
		$smsurl = $this->url.'Credit-Balance/'.$this->token.$this->credit.$credit;
		$result = $this->sendme($smsurl);
		return $result;
	}
}