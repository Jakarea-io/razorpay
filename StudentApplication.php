<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentApplication extends Model
{
    protected $primaryKey = 'student_application_id';
    protected $table = 'students_applications';
    protected $fillable = [
        'first_name'
        ,'middle_name'
        ,'last_name'
        ,'display_name'
        ,'gender'
        ,'date_of_birth'
        ,'email_id'
        ,'alt_email_id'
        ,'profile_image'
        ,'mobile_number'
        ,'blood_group'
        ,'religion'
        ,'community'

        ,'address'
       
        ,'state'
        ,'city'
        ,'pin_code'

        ,'father_name'	
        ,'father_occupation'	
        ,'father_mobile_number'
        ,'mother_name'	
        ,'mother_occupation'	
        ,'mother_mobile_number'
        ,'guardian_name'	
        ,'guardian_occupation'	
        ,'guardian_mobile_number'	
        
        ,'th_board'
        ,'th_name_of_school'
        ,'th_percentage_cgpa'
        ,'th_passing_month'
        ,'th_passing_year'
        ,'th_medium_of_instruction'
        ,'th_passed_in_single_attempt'
        
        ,'tlv_board'
        ,'tlv_name_of_school'
        ,'tlv_percentage_cgpa'
        ,'tlv_passing_month'
        ,'tlv_passing_year'
        ,'tlv_medium_of_instruction'
        ,'tlv_passed_in_single_attempt'

        ,'degree'
        ,'name_of_college'
        ,'name_of_university'
        ,'overall_cgp'
        ,'degree_passing_month'
        ,'degree_passing_year'
        ,'degree_medium_of_instruction'
        ,'other_educational_information'
        ,'ug_time_of_applying'

        // ,'select_test'
        // ,'date_of_test'
        // ,'registration_number'
        // ,'overall_score'
        // ,'overall_percentile'
        ,'campus'
        ,'program_group'
        ,'program'
        ,'batch'
        ,'seat_type'
    ];
}

