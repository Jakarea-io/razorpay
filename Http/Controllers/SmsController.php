<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentApplication;
use Mail;
use App\Mail\ApplicationStatus;
use Auth;

class SmsController extends Controller
{

    public function acceptApplication($id)
    {
		$StudentApplication = StudentApplication::where('student_application_id', $id)->first();
        $StudentApplication->approved_by = Auth::user()->name;
        $StudentApplication->application_status = 'Accepted';
        if($StudentApplication->save()){
			$uploadLink = url('upload-documents?mobile_number='.$StudentApplication->mobile_number.'&date_of_birth='.$StudentApplication->date_of_birth);
            $details = [
				"subject" => "Application Accepted",
				"link"  => $uploadLink,
				"name" => $StudentApplication->first_name ." ". $StudentApplication->last_name,
				"text" => "Greetings from J.J.College, your application has been accepted please visit $uploadLink to upload documents and pay you semester fees.",
				"sms" => "Greetings from J.J.College, your application has been accepted please visit https://bit.ly/application-status to upload documents and pay you semester fees."
			];

            Mail::to($StudentApplication->email_id)->send(new ApplicationStatus($details));
           
            $url = 'http://pay4sms.in';
            $token = 'ad7bf8c43708efbb5f80081ee0104206';
            $credit = '2';
            $sender = 'PAYMSG';
            $message = $details['name'] .' '. $details['sms'];
            $number = $StudentApplication->mobile_number;
            $sendsms = new sendsms($url,$token);
            $message_id = $sendsms->sendmessage($credit,$sender,$message,$number);
            $dlr_status = $sendsms->checkdlr($message_id);
            $available_credit = $sendsms->availablecredit($credit);

        }
        return redirect()->back()->with('success', 'Application has been accepted!');   
	}
	
	
	
	public function verifyDocument($id)
    {
		$StudentApplication = StudentApplication::where('student_application_id', $id)->first();
        $StudentApplication->approved_by = Auth::user()->name;
        $StudentApplication->application_status = 'Verified';
        if($StudentApplication->save()){
            $details = [
				"subject" => "Document Verified",
				"link"  => '',
				"name" => $StudentApplication->first_name ." ". $StudentApplication->last_name,
				"text" => "Greetings from J.J.College, your application documents verification success! You can check by  visiting https://bit.ly/application-status"
			];

            Mail::to($StudentApplication->email_id)->send(new ApplicationStatus($details));
           
            $url = 'http://pay4sms.in';
            $token = 'ad7bf8c43708efbb5f80081ee0104206';
            $credit = '2';
            $sender = 'PAYMSG';
            $message = $details['name'] .' '. $details['text'];
            $number = $StudentApplication->mobile_number;
            $sendsms = new sendsms($url,$token);
            $message_id = $sendsms->sendmessage($credit,$sender,$message,$number);
            $dlr_status = $sendsms->checkdlr($message_id);
            $available_credit = $sendsms->availablecredit($credit);
        }
        return redirect('applications')->with('success', 'Documents has been accepted!');   
	}
	
	public function rejectApplication($id)
    {
        $StudentApplication = StudentApplication::where('student_application_id', $id)->first();
        $StudentApplication->approved_by = Auth::user()->name;
        $StudentApplication->application_status = 'Rejected';
        if($StudentApplication->save()){
			$uploadLink = url('upload-documents?mobile_number='.$StudentApplication->mobile_number.'&date_of_birth='.$StudentApplication->date_of_birth);
            $details = [
				"subject" => "Application Rejected",
				"link"  => $uploadLink,
				"name" => $StudentApplication->first_name ." ". $StudentApplication->last_name,
				"text" => "Your application for ".$StudentApplication->program." - ".$StudentApplication->specific_programme." is rejected."
			];

            Mail::to($StudentApplication->email_id)->send(new ApplicationStatus($details));
            // $number = $StudentApplication->mobile_number;
            // $message = $details = ['text'];
            //$this->smsSender($message,$number);
        }
        return redirect()->back()->with('warning', 'Application has been rejected!');  
    }
}


class sendsms
{
	private $url;
  	private $token;
	private $credit;
	private $message;
	private $number;
	private $sender;
	private $msgid; 
	public function __construct($url,$token)
	{
		$this->url = $url.'/';
		$this->token = '?token='.$token;
		$this->credit = '&credit=';
		$this->sender = '&sender=';
		$this->number = '&number=';
		$this->message = '&message=';
		$this->msgid = '&msgid=';
	}	
	public function __destruct()
	{
	}
	function sendme($smsurl)
	{
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_URL,$smsurl);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_HEADER,false);
       
		$result = curl_exec($curl);
        curl_close($curl);
        
		return $result;
	}
	public function sendmessage($credit,$sender,$message,$number)
	{
		$message = urlencode($message);
		$smsurl = $this->url.'sendsms/'.$this->token.$this->sender.$sender.$this->number.$number.$this->credit.$credit.$this->message.$message;
        $result = $this->sendme($smsurl);
		return $result;
	}
	public function checkdlr($message_id)
	{
		$smsurl = $this->url.'Dlrcheck/'.$this->token.$this->msgid.$message_id;
		$result = $this->sendme($smsurl);
		return $result;
	}
	public function availablecredit($credit)
	{
		$smsurl = $this->url.'Credit-Balance/'.$this->token.$this->credit.$credit;
		$result = $this->sendme($smsurl);
		return $result;
	}
}