<?php

namespace App\Http\Controllers;
use App\StudentApplication;
use App\Document;
use Illuminate\Http\Request;
use Razorpay\Api\Api;
use Illuminate\support\Str;
use DataTables;
use Mail;
use App\Mail\StudentApplicationMail;

class StudentApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
     
   private $rozarpayId = "rzp_live_8xpMuIBTxHKto1";
   private $rozarpayKey = "I6XtOLWdZWXDqY9rikcy35XX";
   
   //private $rozarpayId = "rzp_test_jNmnOqmpjFtjG2";
    //private $rozarpayKey = "Urtcj606HVhrvwbup3fv1bkQ";
    
    // public function testPay(){
    //      $response = [
    //             'orderId' => 22345677,
    //             'rozarpayId' => $this->rozarpayId,
    //             'amount' => 30000,
    //             'name' =>  'Jakarea Parvez',
    //             'currency' => 'INR',
    //             'email' => $StudentApplication->email_id,
    //             'contactNumber' => $StudentApplication->mobile_number,
    //             'dob' => $StudentApplication->date_of_birth,
    //             'address' => $StudentApplication->address,
    //             'Description' => 'Test rozarpay'
    //         ];
    //         return view('application.payment',compact('response'));
    // }

    public function index(Request $request)
    {
        // $applications = StudentApplication::orderBy('created_at', 'desc')->get();
        // return view('application.data',compact('applications'));

        
            if ($request->ajax()) {
                $data = StudentApplication::latest()->get();
                return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                               $btn = '<a href="applications/'.$row->student_application_id.'" class="btn btn-info btn-sm btn-fill pull-right">View</a>';
                                return $btn;
                        })
                      
                        ->editColumn('name', function ($row) {
                            return $row->first_name .' '. $row->last_name;
                       })
                        ->rawColumns(['action'])
                        ->make(true);
            }
          
            return view('application.data');
        
    }

    public function all(Request $request)
    {
        $mobile_number = $request->mobile_number?$request->mobile_number : null;
        $date_of_birth = $request->date_of_birth?$request->date_of_birth : null; 

        $status = StudentApplication::where('mobile_number', $request->mobile_number)
                            ->where('date_of_birth', $request->date_of_birth)->first();
        return view('application.status',compact('status'));
    }

    public function status(Request $request)
    {
        $mobile_number = $request->mobile_number?$request->mobile_number : null;
        $date_of_birth = $request->date_of_birth?$request->date_of_birth : null; 

        $status = StudentApplication::where('mobile_number', $request->mobile_number)
                            ->where('date_of_birth', $request->date_of_birth)->first();
        return view('application.status',compact('status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('application.form2');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    public function store(Request $request)
    {
        $deletePaymentFail = StudentApplication::where('mobile_number', $request->mobile_number)->where('email_id', $request->email_id)->where('razorpay_payment_id',null)->delete();
        $this->validate($request, [
            'first_name' => 'required|string|max:55'
            ,'last_name'=> 'required|string'
           
            ,'gender'=> 'required|string'
            ,'date_of_birth'=> 'required|string'
            ,'email_id'=> 'required|email|unique:students_applications'
          
            ,'alt_email_id' => 'nullable|email'
            ,'mobile_number'=> 'required|unique:students_applications|max:10'
            
            ,'student_aadhar_number' => 'required|string|max:55'
            ,'parent_aadhar_number' => 'required|string|max:55'

            ,'alt_mobile_number'=> 'nullable|max:10'
           
            ,'address' => 'required|string'
          
            ,'state' => 'required|string'
            ,'city' => 'required|string'
            ,'pin_code' => 'required'

            ,'father_name'	=> 'required|string'
            ,'father_occupation' => 'required|string'
            ,'father_mobile_number'	=> 'nullable|max:10'

            ,'mother_name' => 'required|string'
           
            ,'mother_mobile_number' => 'nullable|max:10'

            ,'guardian_mobile_number'	=> 'nullable|max:10'
              

            ,'th_board' 	=> 'required|string'
            ,'th_other_board' =>  'required_if:th_board,Other'
            ,'th_name_of_school' 	=> 'required|string'
            ,'th_percentage_cgpa' 	=> 'required|string'
            ,'th_passing_month' 	=> 'required|string'
            ,'th_passing_year' 	=> 'required|string'
            ,'th_medium_of_instruction' 	=> 'required|string'
            ,'th_passed_in_single_attempt' 	=> 'required|string'
            
            ,'tlv_board' 	=> 'required|string'
            ,'tlv_other_board' =>  'required_if:tlv_board,Other'
            ,'tlv_name_of_school' 	=> 'required|string'
            ,'tlv_percentage_cgpa' 	=> 'required|string'
            ,'tlv_passing_month' 	=> 'required|string'
            ,'tlv_passing_year' 	=> 'required|string'
            ,'tlv_medium_of_instruction' 	=> 'required|string'
            ,'tlv_passed_in_single_attempt' 	=> 'required|string'

            ,'degree' 	=> 'required|string'
            ,'dept_branch_subject' => 'required_if:degree,Others'
            ,'specialization' => 'required_if:degree,BBA,BCA,B.E,B.Tech,BBM,B.Sc,BA'
            ,'name_of_college' 	=> 'required|string'
            ,'name_of_university' 	=> 'required|string'
            ,'overall_cgp' 	=> 'required|string'
            ,'degree_medium_of_instruction' 	=> 'required|string'
            ,'program' 	=> 'required|string'
            ,'specific_programme' 	=> 'required|string'
           
        ]);
        
        $program_fee = [
            'Biochemistry' => 20150,
            'Biotechnology' => 20150,
            'Botany' => 10300,
            'Chemistry' => 15150,
            'Computer Science' => 15150,
            'Information Technology' => 15150,
            'Mathematics' => 11300,
            'Microbiology' => 20150,
            'Physics' => 15150,
            'Gene Technology' => 20150,
            'Environmental Science' => 13650,
            'Industrial Electronics' => 15150,
            'Organic Chemistry' => 20150,
            'Computer Applications' => 25150,
           // 'Computer Applications' => 11650, M.Com
            'Business Administration' => 35150,
            'English' => 10650
        ];
       
        
        $StudentApplication = new StudentApplication;
        $StudentApplication->first_name = Str::of($request->first_name)->upper();
        $StudentApplication->last_name = Str::of($request->last_name)->upper();
        $StudentApplication->middle_name = Str::of($request->middle_name)->upper();
        $StudentApplication->gender = $request->gender;
        $StudentApplication->date_of_birth = $request->date_of_birth;
        $StudentApplication->email_id = $request->email_id;
        $StudentApplication->alt_email_id = $request->alt_email_id;
        $StudentApplication->mobile_number = $request->mobile_number;
        $StudentApplication->alt_mobile_number = $request->alt_mobile_number;
       
        $StudentApplication->student_aadhar_number = $request->student_aadhar_number;
        $StudentApplication->parent_aadhar_number = $request->parent_aadhar_number;

        $StudentApplication->blood_group = $request->blood_group;
        $StudentApplication->religion = $request->religion;
        $StudentApplication->community = $request->community;

        $StudentApplication->address = $request->address;
       
        $StudentApplication->state = $request->state;
        $StudentApplication->city = $request->city;
        $StudentApplication->pin_code = $request->pin_code;

        $StudentApplication->father_name = $request->father_name;
        $StudentApplication->father_occupation = $request->father_occupation;	
        $StudentApplication->father_mobile_number = $request->father_mobile_number;
        $StudentApplication->mother_name = $request->mother_name;
        $StudentApplication->mother_occupation = $request->mother_occupation;	
        $StudentApplication->mother_mobile_number = $request->mother_mobile_number;
        $StudentApplication->guardian_name = $request->guardian_name;
        $StudentApplication->guardian_occupation = $request->guardian_occupation;
        $StudentApplication->guardian_mobile_number = $request->guardian_mobile_number;
        
        $StudentApplication->th_board = $request->th_board;
        $StudentApplication->th_other_board = $request->th_other_board;
        $StudentApplication->th_name_of_school = $request->th_name_of_school;	
        $StudentApplication->th_percentage_cgpa = $request->th_percentage_cgpa;
        $StudentApplication->th_passing_month = $request->th_passing_month;
        $StudentApplication->th_passing_year = $request->th_passing_year;	
        $StudentApplication->th_medium_of_instruction = $request->th_medium_of_instruction;
        $StudentApplication->th_passed_in_single_attempt = $request->th_passed_in_single_attempt;

        $StudentApplication->tlv_board = $request->tlv_board;
        $StudentApplication->tlv_other_board = $request->tlv_other_board;
        $StudentApplication->tlv_name_of_school = $request->tlv_name_of_school;
        $StudentApplication->tlv_percentage_cgpa = $request->tlv_percentage_cgpa;
        $StudentApplication->tlv_passing_month = $request->tlv_passing_month;
        $StudentApplication->tlv_passing_year = $request->tlv_passing_year;
        $StudentApplication->tlv_medium_of_instruction = $request->tlv_medium_of_instruction;
        $StudentApplication->tlv_passed_in_single_attempt = $request->tlv_passed_in_single_attempt;

        $StudentApplication->degree = $request->degree;
        $StudentApplication->dept_branch_subject = $request->dept_branch_subject;
        $StudentApplication->specialization = $request->specialization;
        $StudentApplication->name_of_college = $request->name_of_college;
        $StudentApplication->name_of_university = $request->name_of_university;
        $StudentApplication->overall_cgp = $request->overall_cgp;
        $StudentApplication->degree_passing_month = $request->degree_passing_month;
        $StudentApplication->degree_passing_year = $request->degree_passing_year;
        $StudentApplication->degree_medium_of_instruction = $request->degree_medium_of_instruction;
        $StudentApplication->other_educational_information = $request->other_educational_information;
        $StudentApplication->ug_time_of_applying = $request->ug_time_of_applying;

        // $StudentApplication->select_test = $request->select_test;
        // $StudentApplication->date_of_test = $request->date_of_test;
        // $StudentApplication->registration_number = $request->registration_number;
        // $StudentApplication->overall_score = $request->overall_score;
        // $StudentApplication->overall_percentile = $request->overall_percentile;
        
        $StudentApplication->program = $request->program;
        $StudentApplication->specific_programme = $request->specific_programme;
        if($StudentApplication->program == 'M.Com'){
            $StudentApplication->fee = 11650;
            $StudentApplication->fee_remaining = 11650;
        }else{
            $StudentApplication->fee = $program_fee[$request->specific_programme];
            $StudentApplication->fee_remaining = $program_fee[$request->specific_programme];
        }
        
        //$saved = $StudentApplication->save();
        
        //if(1){
            // $link = url('application-status?mobile_number='.$StudentApplication->mobile_number.'&date_of_birth='.$StudentApplication->date_of_birth);
            // $details = [
            //     "subject" => "Application Success",
            //     "name" => $StudentApplication->first_name .' '. $StudentApplication->last_name,
            //     "text" => "Thanks for applying to JJ College of Arts and Science pudukkottai. You application for ".$request->program." in ".$request->specific_programme." applied successfully submitted. We will update you in 24 hours regarding your application status.
            //     You can also <a href='".$link."'>check your application </a>status <br> Link: ".$link." For any queries contact xxxxxxx"
            // ];

            // $url = 'http://pay4sms.in';
            // $token = 'ad7bf8c43708efbb5f80081ee0104206';
            // $credit = '2';
            // $sender = 'PAYMSG';
            // $message = $details['name'] .' '. $details['text'];
            // $number = $StudentApplication->mobile_number;
            // $sendsms = new sendsms($url,$token);
            // $message_id = $sendsms->sendmessage($credit,$sender,$message,$number);
            // $dlr_status = $sendsms->checkdlr($message_id);
            // $available_credit = $sendsms->availablecredit($credit);

           // Mail::to($StudentApplication->email_id)->send(new StudentApplicationMail($details));
            $api = new Api($this->rozarpayId, $this->rozarpayKey);
            $receiptId = Str::random(16);
            $order = $api->order->create(array(
                'receipt' => $receiptId,
                'amount' => 300 * 100,
                'currency' => 'INR',
                'payment_capture' => 1
            )
        );
        
            $StudentApplication->razorpay_order_id = $order['id'];
            $StudentApplication->amount = 300;
            $saved = $StudentApplication->save();
            //$request->session()->put('StudentApplication', serialize($StudentApplication));

            $response = [
                'orderId' => $order['id'],
                'rozarpayId' => $this->rozarpayId,
                'amount' => 30000,
                'name' =>  $StudentApplication->first_name .' ' . $StudentApplication->last_name,
                'currency' => 'INR',
                'email' => $StudentApplication->email_id,
                'contactNumber' => $StudentApplication->mobile_number,
                'dob' => $StudentApplication->date_of_birth,
                'address' => $StudentApplication->address,
                'Description' => 'Test rozarpay'
            ];
            return view('application.payment',compact('response'));
        // }else{
        //     return redirect()->back()->with('error', 'Something went wrong! Please try again.');
        // }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $application = StudentApplication::find($id);
        $document = Document::where('student_application_id',$id)->first();
        return view('application.single',compact('application','document'));
    }
    
    public function getDocument($id)
    {
        $status = StudentApplication::find($id);
        $document = Document::where('student_application_id',$id)->first();
        return view('application.singleDocument',compact('status','document'));
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

class sendsms
{
	private $url;
  	private $token;
	private $credit;
	private $message;
	private $number;
	private $sender;
	private $msgid; 
	public function __construct($url,$token)
	{
		$this->url = $url.'/';
		$this->token = '?token='.$token;
		$this->credit = '&credit=';
		$this->sender = '&sender=';
		$this->number = '&number=';
		$this->message = '&message=';
		$this->msgid = '&msgid=';
	}	
	public function __destruct()
	{
	}
	function sendme($smsurl)
	{
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_URL,$smsurl);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_HEADER,false);
        //echo $curl;
		$result = curl_exec($curl);
        //print_r($result);
        curl_close($curl);
        
		return $result;
	}
	public function sendmessage($credit,$sender,$message,$number)
	{
		$message = urlencode($message);
		$smsurl = $this->url.'sendsms/'.$this->token.$this->sender.$sender.$this->number.$number.$this->credit.$credit.$this->message.$message;
      //  print_r($smsurl);
        $result = $this->sendme($smsurl);
		return $result;
	}
	public function checkdlr($message_id)
	{
		$smsurl = $this->url.'Dlrcheck/'.$this->token.$this->msgid.$message_id;
		$result = $this->sendme($smsurl);
		return $result;
	}
	public function availablecredit($credit)
	{
		$smsurl = $this->url.'Credit-Balance/'.$this->token.$this->credit.$credit;
		$result = $this->sendme($smsurl);
		return $result;
	}
}