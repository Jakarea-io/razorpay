<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use App\Mail\StudentApplicationMail;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function testMail(){
		$details = [
			'name' => 'Jakarea Parvez',
			'date_of_birth' => '11/12/1993',
			'mobile_number' => '134567654'
		];
		Mail::to('jakareaparvez@gmail.com')->send(new StudentApplicationMail($details));

	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
