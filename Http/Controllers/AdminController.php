<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentApplication;
use Mail;
use App\Mail\ApplicationStatus;

class AdminController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function acceptApplication($id)
    {
        $accepted = StudentApplication::where('student_application_id', $id)
        ->update([
            'approved_by' => 'dummy admin',
            'application_status' => 'Accepted',
            ]);
        if($accepted){

        }
        return redirect()->back()->with('success', 'Application has been accepted!');   
    }

    public function rejectApplication($id)
    {
        StudentApplication::where('student_application_id', $id)
        ->update([
            'approved_by' => 'dummy admin',
            'application_status' => 'Rejected',
            ]);

        $StudentApplication = StudentApplication::where('student_application_id', $id)->first();
        $StudentApplication->approved_by = 'dummy admin';
        $StudentApplication->application_status = 'Rejected';
        if($StudentApplication->save()){
            $details = [
                'status' => 'Rejected'
            ];
            Mail::to($StudentApplication->email_id)->send(new ApplicationStatus($details));
        }
        return redirect()->back()->with('warning', 'Application has been rejected!');   
    }

    public function acceptedList(Request $request)
    {
        return StudentApplication::where('application_status', 'Approved')->get();
       
        return redirect()->back()->with('warning', 'Application has been rejected!');   
    }
    
    public function all(Request $request)
    {
        $all = StudentApplication::orderBy('student_application_id','desc');
        $q = $request->q ? $request->q : '';
        $p = $request->p ? $request->p : '';
        $s = $request->s ? $request->s : '';
        $sp = $request->sp ? $request->sp : '';

        if(!empty($q)){
            $all = $all->orWhere('first_name','like','%'.$q.'%')
            ->orWhere('last_name','like','%'.$q.'%')
            ->orWhere('middle_name','like','%'.$q.'%')
            ->orWhere('email_id','like','%'.$q.'%')
            ->orWhere('mobile_number','like','%'.$q.'%');
        }

        if(!empty($p)){
            $all = $all->where('program',$p);
        }
        if(!empty($sp)){
            $all = $all->where('specific_programme',$sp);
        }
        if(!empty($s)){
            $all = $all->where('application_status',$s);
        }
       
        $all = $all->paginate(16);
        return view('application.all',compact('all'));
    }

    public function selected(Request $request)
    {
        return StudentApplication::where('application_status', 'Approved')->get();
       
        return redirect()->back()->with('warning', 'Application has been rejected!');   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}