@extends('layouts.students')
@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<div class="card">
					<div class="card-header">
						<h4 class="card-title text-center">Application Status</h4>
					</div>
					<div class="card-body label_css">
						<form action="" method="GET">
							<div class="container-fluid">
								<div class="row justify-content-center">
									<div class="col-md-10">
										<div class="card">
											<div class="card-body label_css">
												<div class="row">
                                                    <div class="col-md-12">
                                                    	Dear {{session('details')['name']}}
														<p>
														{{ session('details')['text']}}
														</P>
													</div>
                                                </div>
											</div>
										</div>
										<div class="clearfix">                
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection