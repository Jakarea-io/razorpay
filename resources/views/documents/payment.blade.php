@extends('layouts.students')
@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<div class="card">
					<div class="card-header">
						<h4 class="card-title text-center">Upload Documents</h4>
					</div>
					<div class="card-body label_css">
						<div class="container-fluid">
							<div class="row justify-content-center">
								<div class="col-md-10">
									<!-- Academic Details card start -->
									@isset($application)
									<div class="card">
										<div class="card-body label_css">
											<div class="row">
												<div class="col-md-12">
													<div class="card-head">
														<h6 class="badge badge-pill pil_custom badge-danger">Payment Information</h6>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="program">Paid <span class="text-danger">*</span></label>
														<p class="form-control" >INR {{ $application->amount }} /-</p>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="program_group">Application Status <span class="text-danger">*</span></label>
														<p class="form-control" >{{ $application->application_status ??'pending'}}</p>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="program_group">Program <span class="text-danger">*</span></label>
														<p class="form-control" >{{ $application->program }}</p>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="program_group">Specific Program <span class="text-danger">*</span></label>
														<p class="form-control" >{{ $application->specific_programme }}</p>
													</div>
												</div>
												<div class="col-md-6" style="background: #eae9e5;">
													<div class="form-group">
														<label for="program_group">Program Fee <span class="text-danger">*</span></label>
														<p class="form-control" >{{ $application->fee }}</p>
													</div>
												</div>
												<div class="col-md-6" style="background: #eae9e5;">
													<div class="form-group">
														<label for="program_group">Minimum Payment<span class="text-danger">*</span></label>
														<p class="form-control" >{{ '5000' }}</p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="card-head">
														<h6 class="badge badge-pill pil_custom badge-danger">1. Personal Details</h6>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-4">
													<div class="form-group">
														<label for="first_name">Name <span class="text-danger">*</span></label>
														<p class="uppercase form-control" >{{ $application->first_name }}  {{ $application->last_name }}</p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="first_name">Father Name <span class="text-danger">*</span></label>
														<p class="uppercase form-control" >{{ $application->father_name }}</p>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="last_name">Mother Name <span class="text-danger">*</span></label>
														<p class="form-control" >{{ $application->mother_name }}</p>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="gender">Gender <span class="text-danger">*</span></label>
														<p class="form-control" >{{ $application->gender }}</p>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label for="email_id">Email ID <span class="text-danger">*</span></label>
														<p class="form-control" >{{ $application->email_id }}</p>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h4 class="card-title text-center">
												@if(isset($hasDocuments))
												Pay tuitions fee
												@else
												Upload Documents
												@endif
											</h4>
										</div>
										<div class="card-body label_css">
											@if(isset($hasDocuments))
											<form action="{{ url('pay-now')}}" method="POST">
											
												<div class="row">
													<div class="col-md-6" style="background: #eae9e5;">
													        <div class="form-group">
													            <label for="program_group">Payment Amount <span class="text-danger">*</span></label>
													            <input type="number" required value="5000" class="form-control @error('amount') is-invalid @enderror" placeholder="Pay now" name="amount" id="amount" value="{{ old('amount')}}">
													            @error('amount')
													                <small class="invalid-feedback">{{ $errors->first('amount') }}</small>
													            @enderror
													        </div>
													    </div>
													    <div class="col-md-6" style="background: #eae9e5;">
													        <div class="form-group">
													            <label for="program_group">Remaining Amount<span class="text-danger">*</span></label>
													            <p class="form-control" >{{ $application->fee_remaining }}</p>
													        </div>
													    </div>
													</div>
													</div>
												@else
											<form action="{{ url('upload-documents') }}" method="POST" enctype="multipart/form-data">
												@endif
												@csrf
												<input type="hidden" name="student_application_id" value="{{ $application->student_application_id }}">
											
												@if(!isset($hasDocuments))
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Passport Size Photo</b> <span class="text-danger">*</span></label>
															<input type="file" required id="photo" name="photo" class="form-control-file" />
															<small class="invalid-feedback" id="photoMsg" style="display: block"></small>
															@error('photo')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('photo') }}</small>
															@enderror
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Transfer Certificate</b> <span class="text-danger">*</span></label>
															<input type="file" required id="transfer_certificate" name="transfer_certificate" />
															<small class="invalid-feedback" style="display: block" id="transfer_certificateMsg"></small>
															@error('transfer_certificate')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('transfer_certificate') }}</small>
															@enderror
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Conduct Certificate</b> <span class="text-danger">*</span></label>
															<input type="file" required id="conduct_certificate" name="conduct_certificate" />
															<small class="invalid-feedback" style="display: block" id="conduct_certificateMsg"></small>
															@error('conduct_certificate')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('conduct_certificate') }}</small>
															@enderror
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Mark Sheet</b> <span class="text-danger">*</span></label><br>
															<input type="file" required id="mark_sheet" name="mark_sheet" />
															<small class="invalid-feedback" style="display: block" id="mark_sheetMsg"></small>
															@error('mark_sheet')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('mark_sheet') }}</small>
															@enderror
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Provisional Degree Certificate</b> <span class="text-danger">*</span></label>
															<input type="file" required id="provisional_degree_certificate" name="provisional_degree_certificate" />
															<small class="invalid-feedback" style="display: block" id="provisional_degree_certificateMsg"></small>
															@error('provisional_degree_certificate')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('provisional_degree_certificate') }}</small>
															@enderror
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Aadhar Card Front</b> <span class="text-danger">*</span></label>
															<input type="file" required id="aadhar_card_front" name="aadhar_card_front" />
															<small class="invalid-feedback" style="display: block" id="aadhar_card_frontMsg"></small>
															@error('aadhar_card_front')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('aadhar_card_front') }}</small>
															@enderror
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Aadhar Card Back</b> <span class="text-danger">*</span></label>
															<input type="file" required id="aadhar_card_back" name="aadhar_card_back" />
															<small class="invalid-feedback" style="display: block" id="aadhar_card_backMsg"></small>
															@error('aadhar_card_back')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('aadhar_card_back') }}</small>
															@enderror
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Parent Aadhar Card Front</b> <span class="text-danger">*</span></label>
															<input type="file" required id="parent_aadhar_card_front" name="parent_aadhar_card_front" />
															<small class="invalid-feedback" style="display: block" id="parent_aadhar_card_front"></small>
															@error('parent_aadhar_card_front')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('parent_aadhar_card_front') }}</small>
															@enderror
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="gender"><b>Parent Aadhar Card Back</b> <span class="text-danger">*</span></label>
															<input type="file" required id="parent_aadhar_card_back" name="parent_aadhar_card_back" />
															<small class="invalid-feedback" style="display: block" id="parent_aadhar_card_back"></small>
															@error('parent_aadhar_card_back')
															<small class="invalid-feedback" style="display: block">{{ $errors->first('parent_aadhar_card_back') }}</small>
															@enderror
														</div>
													</div>
												</div>
												@endif
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															@if(isset($hasDocuments))
															<button type="submit" class="btn btn-danger btn-fill ">Pay Now</button> 
															<button type="reset" class="btn btn-secondary ml-2 btn-fill ">Cancel</button>
															@else
															<button type="submit" class="btn btn-danger btn-fill ">Submit</button> 
															<button type="reset" class="btn btn-secondary ml-2 btn-fill ">Reset</button>
															@endif
														</div>
													</div>
												</div>
											</form>
											<div class="clearfix"></div>
										</div>
									</div>
									@endisset
									@empty($application)
									@php if(isset($_GET['mobile_number']) && isset($_GET['date_of_birth'])) 
									echo '<span class="text-danger">No record found</span>'
									@endphp
									@endempty
									<div class="clearfix">                
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection