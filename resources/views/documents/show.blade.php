@extends('layouts.admin')
@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title text-center">Check Documents</h4>
					</div>
					<div class="card-body label_css">
						<form action="{{ url('application-status')}}" method="GET">
							<div class="container-fluid">
								<div class="row justify-content-center">
									<div class="col-md-10">
										<!-- Academic Details card start -->
                                        @isset($status)
										<div class="card">
                                            <div class="card-body label_css">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">Payment Information</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program">Paid <span class="text-danger">*</span></label>
                                                            <p class="form-control" >INR {{ $status->amount }} /-</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Application Status <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->application_status }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Program <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->program }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Specific Program <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->specific_programme }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">1. Personal Details</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="first_name">Name <span class="text-danger">*</span></label>
                                                            <p class="uppercase form-control" >{{ $status->first_name }}  {{ $status->last_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                            <label for="first_name">Father Name <span class="text-danger">*</span></label>
                                                            <p class="uppercase form-control" >{{ $status->father_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="last_name">Mother Name <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->mother_name }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="gender">Gender <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->gender }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                        <label for="email_id">Email ID <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->email_id }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">Photo</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->photo) }}"/></br>
                                                        <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->photo) }}">Downlaod</a>
                                                    </div>
                                                </div>

                                                @if(!empty($document->transfer_certificate) )
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="card-head">
                                                                <h6 class="badge badge-pill pil_custom badge-danger">transfer certificate</h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->transfer_certificate) }}"/></br>
                                                            <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->transfer_certificate) }}">Downlaod</a>
                                                        </div>
                                                    </div>
                                                @endif
                                                @if(!empty($document->conduct_certificate) )
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">conduct certificate</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->conduct_certificate) }}"/></br>
                                                        <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->conduct_certificate) }}">Downlaod</a>
                                                    </div>
                                                </div>
                                                
                                                @endif
                                                @if(!empty($document->mark_sheet) )

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">mark sheet</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->mark_sheet) }}"/></br>
                                                        <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->mark_sheet) }}">Downlaod</a>
                                                    </div>
                                                </div>
                                                @endif
                                                @if(!empty($document->provisional_degree_certificate) )

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">provisional degree certificate</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->provisional_degree_certificate) }}"/></br>
                                                        <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->provisional_degree_certificate) }}">Downlaod</a>
                                                    </div>
                                                </div>

                                                @endif
                                                @if(!empty($document->aadhar_card_front) )

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">aadhar card front</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->aadhar_card_front) }}"/></br>
                                                        <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->aadhar_card_front) }}">Downlaod</a>
                                                    </div>
                                                </div>
                                                @endif
                                                @if(!empty($document->aadhar_card_back) )

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">aadhar card back</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->aadhar_card_back) }}"/></br>
                                                        <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->aadhar_card_back) }}">Downlaod</a>
                                                    </div>
                                                </div>
                                                @endif
                                                @if(!empty($document->parent_aadhar_card_front) )

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">parent aadhar card front</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->parent_aadhar_card_front) }}"/></br>
                                                        <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->parent_aadhar_card_front) }}">Downlaod</a>
                                                    </div>
                                                </div>
                                                @endif
                                                @if(!empty($document->parent_aadhar_card_back) )
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">parent aadhar card back</h6>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img class="img-thumbnail img-responsive" src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/'.$document->parent_aadhar_card_back) }}"/></br>
                                                        <a class="pull-right" href="{{ url('/applications/download/documents?path=documents/'.$document->student_application_id.'/'.$document->parent_aadhar_card_back) }}">Downlaod</a>
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="clearfix"></div>
                                                @if($status->application_status == 'Verify pending')
                                                    <a href="{{ url('/applications/'.$status->student_application_id.'/documents/accept')}}" class="btn btn-success btn-fill ">Verify</a> 
                                                @endif
                                            </div>
                                        </div>								
                                        @endisset
                                        @empty($status)
                                            @php if(isset($_GET['mobile_number']) && isset($_GET['date_of_birth'])) 
                                                echo '<span class="text-danger">No record found</span>'
                                            @endphp
                                        @endempty
										<div class="clearfix">                
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection