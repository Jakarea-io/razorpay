<button style="display:none" id="rzp-button1">Pay</button>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    
var options = {
    "key": "{{ $response['rozarpayId']}}", // Enter the Key ID generated from the Dashboard
    "amount": "{{ $response['amount']}}", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "{{ $response['currency']}}",
    "name": "{{ $response['name']}}",
    "description": "{{ $response['address']}}",
    "image": "http://application.kvet.in/public/assets/img/logo.png",
    "order_id": "{{ $response['orderId']}}", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    "handler": function (response){
        document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id
        document.getElementById('razorpay_order_id').value = response.razorpay_order_id
        document.getElementById('razorpay_signature').value = response.razorpay_signature
        document.getElementById('payment_success').click()
        
        // alert(response.razorpay_payment_id);
        // alert(response.razorpay_order_id);
        // alert(response.razorpay_signature)
    },
    "prefill": {
        "name": "{{ $response['name']}}",
        "email": "{{ $response['email']}}",
        "contact": "{{ $response['contactNumber']}}"
    },
    "notes": {
        "address": "{{ $response['address']}}"
    },
    "theme": {
        "color": "#F37254"
    }
};
var rzp1 = new Razorpay(options);
window.onload = function(){
    document.getElementById('rzp-button1').click()
}
document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
</script>

<form action="payment-complete" method="post" hidden>
    @csrf
    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id">
    <input type="hidden" name="razorpay_order_id" id="razorpay_order_id">
    <input type="hidden" name="razorpay_signature" id="razorpay_signature">
    <button type="submit" id="payment_success">success</button>
</form>