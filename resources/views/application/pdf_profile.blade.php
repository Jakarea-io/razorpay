<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Download applicants profile as pdf</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    tr.item.last {
        height: 50px;
    }
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <img src="{{ public_path('assets/img/logo.png') }}" style="width:100%; max-width:100px;">
                            </td>
                            <td>
                            <h1>J.J. College of Arts & Science</h1>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            @if(!empty($document))
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr >
                            <td></td>
                            <td style="float:right">
                            <img src="{{ public_path('storage/app/public/documents/'.$document->student_application_id.'/'.$document->photo) }}" style="width:100%; max-width:300px;"></br>                  
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            @endif
            <tr class="heading">
                <td>
                    Personal Details
                </td>
                
                <td>
                    
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Name
                </td>
                
                <td>
                {{ $application->first_name }} {{ $application->first_name }}  {{ $application->first_name }} 
                </td>
            </tr>
            <tr class="item">
                <td>
                Gender
                </td>
                
                <td>
                {{ $application->gender }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Date of Birth
                </td>
                
                <td>
                {{ $application->date_of_birth }}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                Email ID
                </td>
                
                <td>
                {{ $application->email_id }}
                </td>
            </tr>
            
            <tr class="item">
                <td>
                Mobile Number
                </td>
                
                <td>
                {{ $application->mobile_number? '+91 '.$application->mobile_number : '' }}
                {{ $application->alt_mobile_number? ' ,+91 '.$application->alt_mobile_number : '' }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Aadhar Number
                </td>
                
                <td>
                {{ $application->student_aadhar_number }}
                </td>
            </tr>

            <tr class="item">
                <td>
                Nationality
                </td>
                
                <td>
                {{ $application->nationality }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Blood Group
                </td>
                
                <td>
                {{ $application->blood_group }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Religion
                </td>
                
                <td>
                {{ $application->religion }}
                </td>
            </tr>
            <tr class="item last">
                <td>
                Community
                </td>
                
                <td>
                {{ $application->community }}
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                CONTACT INFORMATION
                </td>
                
                <td>
                
                </td>
            </tr>

            <tr class="item">
                <td>
                Address
                </td>
                
                <td>
                {{ $application->address }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Country
                </td>
                
                <td>
                {{ $application->country }}
                </td>
            </tr>
            <tr class="item">
                <td>
                State
                </td>
                
                <td>
                {{ $application->state }}
                </td>
            </tr>
            <tr class="item">
                <td>
                City
                </td>
                
                <td>
                {{ $application->city }}
                </td>
            </tr>
            <tr class="item last">
                <td>
                Pin Code
                </td>
                
                <td>
                {{ $application->pin_code }}
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                PARENTS / GUARDIAN DETAILS
                </td>
                
                <td>
                
                </td>
            </tr>

            <tr class="item">
                <td>
                Father Name
                </td>
                
                <td>
                {{ $application->father_name }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Father Occupation
                </td>
                
                <td>
                {{ $application->father_occupation }}
                </td>
            </tr>
            <tr class="item last">
                <td>
                Mother Name
                </td>
                
                <td>
                {{ $application->mother_name }}
                </td>
            </tr>

            <tr class="heading">
                <td>
                    ACADEMIC DETAILS(Under Graduate Degree)
                </td>
                
                <td>
                
                </td>
            </tr>

            <tr class="item">
                <td>
                    Degree
                </td>
                
                <td>
                {{ $application->degree }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Specialization
                </td>
                
                <td>
                {{ $application->specialization }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Department/Branch/Subject
                </td>
                
                <td>
                {{ $application->dept_branch_subject }}
                </td>
            </tr>
            <tr class="item ">
                <td>
                Name of the College
                </td>
                
                <td>
                {{ $application->name_of_college }}
                </td>
            </tr>
            <tr class="item">
                <td>
                    Name of the University
                </td>
                
                <td>
                {{ $application->name_of_university }}
                </td>
            </tr>
            <tr class="item">
                <td>
                Overall %/CGPA <small>(up to pre-final/final Sem)</small>
                </td>
                
                <td>
                    {{ $application->overall_cgp }}
                </td>
            </tr>
            <tr class="item">
                <td>
                    Month &amp; Year of Passing
                </td>
                
                <td>
                {{ $application->degree_passing_month }},{{ $application->degree_passing_year }}
                </td>
            </tr>
            <tr class="item last">
                <td>
                Medium of Instruction
                </td>
                
                <td>
                    {{ $application->degree_medium_of_instruction }}
                </td>
            </tr>
            <tr class="heading">
                <td>
                Program Selection
                </td>
                
                <td>
                
                </td>
            </tr>

            <tr class="item">
                <td>
                Programme
                </td>
                
                <td>
                {{ $application->program }}
                </td>
            </tr>
            <tr class="item last">
                <td>
                Specific programme
                </td>
                
                <td>
                {{ $application->specific_programme }}
                </td>
            </tr>
           
        </table>
    </div>
</body>
</html>