@extends('layouts.admin')
@section('content')
<div class="col-md-12">
	<div class="card strpied-tabled-with-hover">
		<div class="card-header ">
			<h4 class="card-title">List of application</h4>
			
		</div>
		
		<div class="card-body table-full-width table-responsive">
			
			<table class="table table-striped data-table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Mobile No</th>
					<th>Email</th>
					<th>Program</th>
					<th>Specific Program</th>
					<th>Payment Id</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
        <tbody>
        </tbody>
    </table>

		</div>
	</div>
</div>
@endsection