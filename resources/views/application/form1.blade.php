@extends('layouts.students')
@section('content')
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- @if ($errors->any())
                    <div class="alert alert-danger">
                    	<ul>
                    		@foreach ($errors->all() as $error)
                    		<li>{{ $error }}</li>
                    		@endforeach
                    	</ul>
                    </div>
                    @endif -->
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title text-center">Online Application Form</h4>
                    </div>
                    <div class="card-body label_css">
                        <form action="{{ url('online-application')}} " method="post">
                            @csrf
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-md-10">
                                        <!-- first card start -->
                                        <div class="card">
                                            <div class="card-body label_css">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">1. Personal Details</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="first_name">first name <span class="text-danger">*</span></label>
                                                            <input type="text" class="uppercase form-control error_icon @error('first_name') is-invalid @enderror" placeholder="First Name" value="{{ old('first_name')}}" name="first_name" id="first_name">
                                                            @error('first_name')
                                                            <small class="invalid-feedback">{{ $errors->first('first_name') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="middle_name">middle name</label>
                                                            <input type="text" class="uppercase form-control @error('middle_name') is-invalid @enderror" placeholder="Middle Name" name="middle_name" id="middle_name" value="{{ old('middle_name')}}">
                                                            @error('middle_name')
                                                            <small class="invalid-feedback">{{ $errors->first('middle_name') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="last_name">Last name <span class="text-danger">*</span></label>
                                                            <input type="text" class="uppercase form-control error_icon @error('last_name') is-invalid @enderror" placeholder="Last Name" name="last_name" id="last_name" value="{{ old('last_name')}}">
                                                            @error('last_name')
                                                            <small class="invalid-feedback">{{ $errors->first('last_name') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="gender">Gender <span class="text-danger">*</span></label>
                                                            <select class="form-control @error('gender') is-invalid @enderror" name="gender" id="gender">
                                                                <option  value="">--select--</option>
                                                                <option @if(old('gender') == 'Male') selected @endif value="Male">Male</option>
                                                                <option @if(old('gender') == 'Female') selected @endif value="Female">Female</option>
                                                            </select>
                                                            @error('gender')
                                                            <small class="invalid-feedback">{{ $errors->first('gender') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="date_of_birth">Date of Birth <span class="text-danger">*</span></label>
                                                            <input type="date" class="form-control @error('date_of_birth') is-invalid @enderror" placeholder="Date of Birth " name="date_of_birth" value="{{ old('date_of_birth')}}">
                                                            @error('date_of_birth')
                                                            <small class="invalid-feedback">
                                                            {{ $errors->first('date_of_birth') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="email_id">Email ID <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('email_id') is-invalid @enderror" placeholder="Email ID" name="email_id" id="email_id" value="{{ old('email_id')}}">
                                                            @error('email_id')
                                                            <small class="invalid-feedback">{{ $errors->first('email_id') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="alt_email_id">Alternate Email ID </label>
                                                            <input type="text" class="form-control" placeholder="Alternate Email ID " name="alt_email_id" id="alt_email_id" value="{{ old('alt_email_id')}}">
                                                            @error('alt_email_id')
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('alt_email_id') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="mobile_number">Mobile Number <span class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">+91</span>	
                                                                <input type="number" class="form-control lengthC error_icon @error('mobile_number') is-invalid @enderror" placeholder="Mobile Number" name="mobile_number" id="mobile_number" value="{{ old('mobile_number')}}">
                                                            </div>
                                                            @error('mobile_number')                      
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('mobile_number') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="alt_mobile_number">Alternate Mobile Number </label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">+91</span>	
                                                                <input type="number" class="form-control lengthC error_icon @error('alt_mobile_number') is-invalid @enderror" placeholder="Alt Mobile Number" name="alt_mobile_number" id="alt_mobile_number" value="{{ old('alt_mobile_number')}}">
                                                            </div>
                                                            @error('alt_mobile_number')                      
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('alt_mobile_number') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="student_aadhar_number">Student Aadhar Number <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control @error('student_aadhar_number') is-invalid @enderror" placeholder="Your aadhar card number" value="{{ old('student_aadhar_number')}}" name="student_aadhar_number" id="student_aadhar_number">
                                                            @error('student_aadhar_number')
                                                            <small class="invalid-feedback">{{ $errors->first('student_aadhar_number') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="parent_aadhar_number">Parent Aadhar Number <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control @error('parent_aadhar_number') is-invalid @enderror" placeholder="Parent aadhar card number" value="{{ old('parent_aadhar_number')}}" name="parent_aadhar_number" id="parent_aadhar_number">
                                                            @error('parent_aadhar_number')
                                                            <small class="invalid-feedback">{{ $errors->first('parent_aadhar_number') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="nationality">Nationality </label>
                                                            <input type="text" class="form-control" placeholder="Indian" value="Indian" disabled name="nationality" id="nationality" value="{{ old('nationality')}}">
                                                            @error('nationality')                      
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('nationality') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="blood_group">Blood Group</label>
                                                            <select class="form-control" id="blood_group" name="blood_group">
                                                                <option  value="">--select--</option>
                                                                <option @if(old('blood_group') == 'O+') selected @endif value="O+">O+</option>
                                                                <option @if(old('blood_group') == 'O-') selected @endif value="O-">O-</option>
                                                                <option @if(old('blood_group') == 'A+') selected @endif value="A+">A+</option>
                                                                <option @if(old('blood_group') == 'A-') selected @endif  value="A-">A-</option>
                                                                <option @if(old('blood_group') == 'B+') selected @endif value="B+">B+</option>
                                                                <option @if(old('blood_group') == 'B-') selected @endif value="B-">B-</option>
                                                                <option @if(old('blood_group') == 'AB+') selected @endif value="AB+">AB+</option>
                                                                <option @if(old('blood_group') == 'AB-') selected @endif value="AB-">AB-</option>
                                                            </select>
                                                            @error('blood_group')                      
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('blood_group') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="religion">Religion</label>
                                                            <select class="form-control" id="religion" name="religion">
                                                                <option value="">--select--</option>
                                                                <option @if(old('religion') == 'Christian') selected @endif value="Christian">Christian </option>
                                                                <option @if(old('religion') == 'Hindu') selected @endif value="Hindu">Hindu </option>
                                                                <option @if(old('religion') == 'Muslim') selected @endif value="Muslim">Muslim </option>
                                                                <option @if(old('religion') == 'Roman Catholic') selected @endif value="Roman Catholic">Roman Catholic </option>
                                                                <option @if(old('religion') == 'Sikh') selected @endif value="Sikh">Sikh </option>
                                                            </select>
                                                            @error('religion')                      
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('religion') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="community">Community</label>
                                                            <select class="form-control" id="community" name="community">
                                                                <option value="">--select--</option>
                                                                <option @if(old('community') == 'BC') selected @endif value="BC">BC</option>
                                                                <option @if(old('community') == 'Gen') selected @endif value="Gen">Gen</option>
                                                                <option @if(old('community') == 'MBC') selected @endif value="MBC">MBC</option>
                                                                <option @if(old('community') == 'OBC') selected @endif value="OBC">OBC</option>
                                                                <option @if(old('community') == 'OC') selected @endif value="OC">OC</option>
                                                                <option @if(old('community') == 'SC') selected @endif value="SC">SC</option>
                                                                <option @if(old('community') == 'ST') selected @endif value="ST">ST</option>
                                                            </select>
                                                            @error('community')                      
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('community') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <!-- first card end -->
                                        <!-- second card start -->
                                        <div class="card">
                                            <div class="card-body label_css">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">2. Contact Information</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="address">Address<span class="text-danger">*</span></label>
                                                            <textarea name="address" class="form-control error_icon @error('address') is-invalid @enderror"  id="address" cols="30" rows="30" Placeholder="Address" > {{ old('address')}} </textarea>
                                                            @error('address')
                                                            <small class="invalid-feedback">{{ $errors->first('address') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="country">Country</label>
                                                            <input type="text" class="form-control" disabled placeholder="Country" value="India" name="country" id="country" value="{{ old('country')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="state">State <span class="text-danger">*</span></label>
                                                            <select class="form-control @error('state') is-invalid @enderror" id="state" name="state">
                                                                <option value="">--select--</option>
                                                                <option @if(old('state') == 'Andhra Pradesh (AP)') selected @endif value="Andhra Pradesh (AP)">Andhra Pradesh (AP) </option>
                                                                <option @if(old('state') == 'Arunachal Pradesh (AR)') selected @endif value="Arunachal Pradesh (AR)">Arunachal Pradesh (AR)</option>
                                                                <option @if(old('state') == 'Assam (AS)') selected @endif value="Assam (AS)">Assam (AS) </option>
                                                                <option @if(old('state') == 'Bihar (BR)') selected @endif value="Bihar (BR)">Bihar (BR) </option>
                                                                <option @if(old('state') == 'Chandigarh')' selected @endif value="Chandigarh">Chandigarh</option>
                                                                <option @if(old('state') == 'Chhattisgarh (CG)') selected @endif value="Chhattisgarh (CG)">Chhattisgarh (CG) </option>
                                                                <option @if(old('state') == 'Dadra and Nagar Haveli (DN)') selected @endif value="Dadra and Nagar Haveli (DN)">Dadra and Nagar Haveli (DN) </option>
                                                                <option @if(old('state') == 'Daman and Diu (DD)') selected @endif value="Daman and Diu (DD)">Daman and Diu (DD)</option>
                                                                <option @if(old('state') == 'Delhi (DL)') selected @endif value="Delhi (DL)">Delhi (DL) </option>
                                                                <option @if(old('state') == 'Goa (GA)') selected @endif value="Goa (GA)">Goa (GA) </option>
                                                                <option @if(old('state') == 'Gujarat (GJ)') selected @endif value="Gujarat (GJ)">Gujarat (GJ)</option>
                                                                <option @if(old('state') == 'Haryana (HR)') selected @endif value="Haryana (HR)">Haryana (HR) </option>
                                                                <option @if(old('state') == 'Himachal Pradesh (HP)') selected @endif value="Himachal Pradesh (HP)">Himachal Pradesh (HP) </option>
                                                                <option @if(old('state') == 'Jammu and Kashmir (JK)') selected @endif value="Jammu and Kashmir (JK)">Jammu and Kashmir (JK)</option>
                                                                <option @if(old('state') == 'Jharkhand (JH)') selected @endif value="Jharkhand (JH)">Jharkhand (JH) </option>
                                                                <option @if(old('state') == 'Karnataka (KA)') selected @endif value="Karnataka (KA)">Karnataka (KA) </option>
                                                                <option @if(old('state') == 'Kerala (KL)') selected @endif value="Kerala (KL)">Kerala (KL) </option>
                                                                <option @if(old('state') == 'Madhya Pradesh (MP)') selected @endif value="Madhya Pradesh (MP)">Madhya Pradesh (MP) </option>
                                                                <option @if(old('state') == 'Maharashtra (MH)') selected @endif value="Maharashtra (MH)">Maharashtra (MH) </option>
                                                                <option @if(old('state') == 'Manipur (MN)') selected @endif value="Manipur (MN)">Manipur (MN) </option>
                                                                <option @if(old('state') == 'Meghalaya (ML)') selected @endif value="Meghalaya (ML)">Meghalaya (ML) </option>
                                                                <option @if(old('state') == 'Mizoram (MZ)') selected @endif value="Mizoram (MZ)">Mizoram (MZ) </option>
                                                                <option @if(old('state') == 'Nagaland (NL)') selected @endif value="Nagaland (NL)">Nagaland (NL) </option>
                                                                <option @if(old('state') == 'Orissa (OR)') selected @endif value="Orissa (OR)">Orissa (OR) </option>
                                                                <option @if(old('state') == 'Pondicherry (Puducherry) (PY)') selected @endif value="Pondicherry (Puducherry) (PY)">Pondicherry (Puducherry) (PY) </option>
                                                                <option @if(old('state') == 'Punjab (PB)') selected @endif value="Punjab (PB)">Punjab (PB) </option>
                                                                <option @if(old('state') == 'Rajasthan (RJ)') selected @endif value="Rajasthan (RJ)">Rajasthan (RJ) </option>
                                                                <option @if(old('state') == 'JSikkim (SK)') selected @endif value="JSikkim (SK)">JSikkim (SK) </option>
                                                                <option @if(old('state') == 'Tamil Nadu (TN)') selected @endif value="Tamil Nadu (TN)">Tamil Nadu (TN) </option>
                                                                <option @if(old('state') == 'Tripura (TR)') selected @endif value="Tripura (TR)">Tripura (TR) </option>
                                                                <option @if(old('state') == 'Uttar Pradesh (UP)') selected @endif value="Uttar Pradesh (UP)">Uttar Pradesh (UP) </option>
                                                                <option @if(old('state') == 'Uttarakhand (UK)') selected @endif value="Uttarakhand (UK)">Uttarakhand (UK) </option>
                                                                <option @if(old('state') == 'West Bengal (WB)') selected @endif value="West Bengal (WB)">West Bengal (WB) </option>
                                                            </select>
                                                            @error('state')
                                                            <small class="invalid-feedback">{{ $errors->first('state') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="city">City <span class="text-danger">*</span></label>
                                                            <select class="form-control @error('city') is-invalid @enderror" name="city" id="city">
                                                            @if(old('city'))
                                                                <option value="{{ old('city') }}">{{ old('city') }} </option>
                                                            @else
                                                                <option value="">--select--</option>
                                                            @endif
                                                            </select>
                                                            @error('city')
                                                            <small class="invalid-feedback">{{ $errors->first('city') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="pin_code">Pin Code<span class="text-danger">*</span></label>
                                                            <input type="number" class="form-control error_icon @error('pin_code') is-invalid @enderror" placeholder="Pin Code" name="pin_code" id="pin_code" value="{{ old('pin_code')}}">
                                                            @error('pin_code')
                                                            <small class="invalid-feedback">{{ $errors->first('pin_code') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- second card end -->
                                        <!-- third card start -->
                                        <div class="card">
                                            <div class="card-body label_css">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">3. Parents / Guardian Details</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="father_name">Father Name <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('father_name') is-invalid @enderror" placeholder="Father Name" name="father_name" id="father_name" value="{{ old('father_name')}}">
                                                            @error('father_name')
                                                            <small class="invalid-feedback">{{ $errors->first('father_name') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="father_occupation">Father Occupation <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('father_occupation') is-invalid @enderror" placeholder="Father Occupation" name="father_occupation" id="father_occupation" value="{{ old('father_occupation')}}">
                                                            @error('father_occupation')
                                                            <small class="invalid-feedback">{{ $errors->first('father_occupation') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="father_mobile_number">Father Mobile Number</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">+91</span>	
                                                                <input type="number" class="form-control lengthC @error('father_mobile_number') is-invalid @enderror" placeholder="Mobile Number" name="father_mobile_number" id="father_mobile_number" value="{{ old('father_mobile_number')}}">
                                                            </div>
                                                            @error('father_mobile_number')
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('father_mobile_number') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="mother_name">Mother Name <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('mother_name') is-invalid @enderror" placeholder="Mother Name" name="mother_name" id="mother_name" value="{{ old('mother_name')}}">
                                                            @error('mother_name')
                                                            <small class="invalid-feedback"  style="display:block">{{ $errors->first('mother_name') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="father_occupation">Mother Occupation </label>
                                                            <input type="text" class="form-control" placeholder="Father Occupation" name="mother_occupation" id="mother_occupation" value="{{ old('mother_occupation')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="mother_mobile_number">Mother Mobile Number</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">+91</span>	
                                                                <input type="number" class="form-control error_icon lengthC @error('mother_mobile_number') is-invalid @enderror" placeholder="Mobile Number" name="mother_mobile_number" id="mother_mobile_number" value="{{ old('mother_mobile_number')}}">
                                                            </div>
                                                            @error('mother_mobile_number')
                                                            <small class="invalid-feedback"  style="display:block">{{ $errors->first('mother_mobile_number') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="mother_name">Guardian Name </label>
                                                            <input type="text" class="form-control" placeholder="Guardian Name" name="guardain_name" id="guardain_name" value="{{ old('guardain_name')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="father_occupation">Guardian Occupation </label>
                                                            <input type="text" class="form-control" placeholder="Guardian Occupation" name="guardain_occupation" id="guardain_occupation" value="{{ old('guardain_occupation')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="guardian_mobile_number">Guardian Mobile Number</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">+91</span>	
                                                                <input type="number"  class="form-control error_icon lengthC @error('guardian_mobile_number') is-invalid @enderror" placeholder="Mobile Number" name="guardian_mobile_number" id="guardian_mobile_number" value="{{ old('guardian_mobile_number')}}">
                                                            </div>
                                                            @error('guardian_mobile_number')
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('guardian_mobile_number') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- third card end -->
                                        <!-- Academic Details card start -->
                                        <div class="card">
                                            <div class="card-body label_css">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">4. Academic Details</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- 10th Standard start -->
                                                <div class="row custom_bg">
                                                    <div class="col-md-12">
                                                        <div class="head_custom">
                                                            <h5>10th Standard</h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="th_board">Board <span class="text-danger">*</span></label>
                                                            <select class="getOtherTh form-control @error('th_board') is-invalid @enderror" id="th_board" name="th_board">
                                                                <option value="">--select--</option>
                                                                <option @if(old('th_board') == 'State Board') selected @endif value="State Board">State Board</option>
                                                                <option @if(old('th_board') == 'CBSE') selected @endif value="CBSE">CBSE </option>
                                                                <option @if(old('th_board') == 'ICSC') selected @endif value="ICSC">ICSC </option>
                                                                <option @if(old('th_board') == 'Other') selected @endif value="Other">Other</option>
                                                            </select>
                                                            @error('th_board')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="otherTh" style="display:none">
                                                        <div class="form-group">
                                                            <label for="th_other_board">Please Specify Other Board  <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('th_other_board') is-invalid @enderror" placeholder="Name of other board" name="th_other_board" id="th_other_board" value="{{ old('th_other_board')}}">
                                                            @error('th_other_board')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="th_name_of_school">Name of the School  <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('th_name_of_school') is-invalid @enderror" placeholder="Name of the School" name="th_name_of_school" id="th_name_of_school" value="{{ old('th_name_of_school')}}">
                                                            @error('th_name_of_school')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="th_percentage_cgpa">Percentage /CGPA <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('th_percentage_cgpa') is-invalid @enderror" placeholder="Percentage /CGPA" name="th_percentage_cgpa" id="th_percentage_cgpa" value="{{ old('th_percentage_cgpa')}}">
                                                            @error('th_percentage_cgpa')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="th_passing_month">Month &amp; Year of Passing <span class="text-danger">*</span></label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <select class="form-control @error('th_passing_month') is-invalid @enderror" id="th_passing_month" name="th_passing_month">
                                                                        <option  value="">--select--</option>
                                                                        <option @if(old('th_passing_month') == 'January') selected @endif value="January">January</option>
                                                                        <option @if(old('th_passing_month') == 'February') selected @endif value="February">February </option>
                                                                        <option @if(old('th_passing_month') == 'March') selected @endif value="March">March</option>
                                                                        <option @if(old('th_passing_month') == 'April') selected @endif value="April">April </option>
                                                                        <option @if(old('th_passing_month') == 'May') selected @endif value="May">May </option>
                                                                        <option @if(old('th_passing_month') == 'June') selected @endif value="June">June </option>
                                                                        <option @if(old('th_passing_month') == 'July') selected @endif value="July">July </option>
                                                                        <option @if(old('th_passing_month') == 'August') selected @endif value="August">August </option>
                                                                        <option @if(old('th_passing_month') == 'September') selected @endif value="September">September </option>
                                                                        <option @if(old('th_passing_month') == 'October') selected @endif value="October">October </option>
                                                                        <option @if(old('th_passing_month') == 'November') selected @endif value="November">November </option>
                                                                        <option @if(old('th_passing_month') == 'December') selected @endif value="December">December </option>
                                                                    </select>
                                                                    @error('th_passing_month')
                                                                    <small class="invalid-feedback">This field is required!</small>
                                                                    @enderror
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <select class="form-control @error('th_passing_year') is-invalid @enderror" id="10th_passing_year" name="th_passing_year">
                                                                        <option  value="">--select--</option>
                                                                        <option  @if(old('th_passing_year') == '2025') selected @endif value="2025">2025</option>
                                                                        <option  @if(old('th_passing_year') == '2024') selected @endif value="2024">2024</option>
                                                                        <option  @if(old('th_passing_year') == '2023') selected @endif value="2023">2023</option>
                                                                        <option  @if(old('th_passing_year') == '2022') selected @endif value="2022">2022</option>
                                                                        <option  @if(old('th_passing_year') == '2021') selected @endif value="2021">2021</option>
                                                                        <option  @if(old('th_passing_year') == '2020') selected @endif value="2020">2020</option>
                                                                        <option  @if(old('th_passing_year') == '2019') selected @endif value="2019">2019</option>
                                                                        <option  @if(old('th_passing_year') == '2018') selected @endif value="2018">2018</option>
                                                                        <option  @if(old('th_passing_year') == '2017') selected @endif value="2017">2017</option>
                                                                        <option  @if(old('th_passing_year') == '2016') selected @endif value="2016">2016</option>
                                                                        <option  @if(old('th_passing_year') == '2015') selected @endif value="2015">2015</option>
                                                                        <option  @if(old('th_passing_year') == '2014') selected @endif value="2014">2014</option>
                                                                        <option  @if(old('th_passing_year') == '2013') selected @endif value="2013">2013</option>
                                                                        <option  @if(old('th_passing_year') == '2012') selected @endif value="2012">2012</option>
                                                                        <option  @if(old('th_passing_year') == '2012') selected @endif value="2011">2011</option>
                                                                        <option  @if(old('th_passing_year') == '2010') selected @endif value="2010">2010</option>
                                                                        <option  @if(old('th_passing_year') == '2009') selected @endif value="2009">2009</option>
                                                                        <option  @if(old('th_passing_year') == '2008') selected @endif  value="2008">2008</option>
                                                                        <option  @if(old('th_passing_year') == '2007') selected @endif  value="2007">2007</option>
                                                                        <option  @if(old('th_passing_year') == '2006') selected @endif  value="2006">2006</option>
                                                                        <option  @if(old('th_passing_year') == '2005') selected @endif  value="2005">2005</option>
                                                                        <option  @if(old('th_passing_year') == '2004') selected @endif  value="2004">2004</option>
                                                                        <option  @if(old('th_passing_year') == '2003') selected @endif  value="2003">2003</option>
                                                                        <option  @if(old('th_passing_year') == '2002') selected @endif  value="2002">2002</option>
                                                                        <option  @if(old('th_passing_year') == '2001') selected @endif  value="2001">2001</option>
                                                                        <option  @if(old('th_passing_year') == '2000') selected @endif  value="2000">2000</option>
                                                                        <option  @if(old('th_passing_year') == '1999') selected @endif value="1999">1999</option>
                                                                        <option  @if(old('th_passing_year') == '1998') selected @endif value="1998">1998</option>
                                                                        <option  @if(old('th_passing_year') == '1997') selected @endif value="1997">1997</option>
                                                                        <option  @if(old('th_passing_year') == '1996') selected @endif value="1996">1996</option>
                                                                        <option  @if(old('th_passing_year') == '1995') selected @endif value="1995">1995</option>
                                                                    </select>
                                                                    @error('th_passing_year')
                                                                    <small class="invalid-feedback">This field is required!</small>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="th_medium_of_instruction">Medium of Instruction <span class="text-danger">*</span></label>
                                                            <select class="form-control @error('th_medium_of_instruction') is-invalid @enderror" id="th_medium_of_instruction" name="th_medium_of_instruction">
                                                                <option value="">--select--</option>
                                                                <option @if(old('th_medium_of_instruction') == 'English') selected @endif  value="English">English</option>
                                                                <option @if(old('th_medium_of_instruction') == 'Regional Language') selected @endif  value="Regional Language">Regional Language</option>
                                                            </select>
                                                            @error('th_medium_of_instruction')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="th_passed_in_single_attempt">Passed in Single Attempt <span class="text-danger">*</span></label>
                                                            <br>{{ old('th_passed_in_single_attempt')}}
                                                            <input type="radio" @if(old('th_passed_in_single_attempt') == 'Yes') checked @endif name="th_passed_in_single_attempt" id="Yes" class="@error('th_passed_in_single_attempt') is-invalid @enderror" value="Yes"> <label for="Yes"> Yes</label>
                                                            <input type="radio" @if(old('th_passed_in_single_attempt') == 'No') checked @endif name="th_passed_in_single_attempt" id="No" class="@error('th_passed_in_single_attempt') is-invalid @enderror" value="No"> <label for="No"> No</label>
                                                            @error('th_passed_in_single_attempt')
                                                            <small class="invalid-feedback" style="display: block">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- 10th Standard end -->
                                                <!-- 12th Standard / Diploma start -->
                                                <div class="row custom_bg">
                                                    <div class="col-md-12">
                                                        <div class="head_custom">
                                                            <h5>12th Standard / Diploma</h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="tlv_board">Board <span class="text-danger">*</span></label>
                                                            <select class="getOtherTlv form-control @error('tlv_board') is-invalid @enderror" id="tlv_board" name="tlv_board">
                                                                <option value="">--select--</option>
                                                                <option @if(old('tlv_board') == 'State Board') selected @endif value="State Board">State Board</option>
                                                                <option @if(old('tlv_board') == 'CBSE') selected @endif value="CBSE">CBSE </option>
                                                                <option @if(old('tlv_board') == 'ICSC') selected @endif value="ICSC">ICSC </option>
                                                                <option @if(old('tlv_board') == 'Other') selected @endif value="Other">Other</option>
                                                            </select>
                                                            @error('tlv_board')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="otherTlv" style="display:none">
                                                        <div class="form-group">
                                                            <label for="tlv_other_board">Please Specify Other Board  <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('tlv_other_board') is-invalid @enderror" placeholder="Name of other board" name="tlv_other_board" id="tlv_other_board" value="{{ old('tlv_other_board')}}">
                                                            @error('tlv_other_board')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="tlv_name_of_school">Name of the School  <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('tlv_name_of_school') is-invalid @enderror" placeholder="Name of the School" name="tlv_name_of_school" id="tlv_name_of_school" value="{{ old('tlv_name_of_school')}}">
                                                            @error('tlv_name_of_school')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="tlv_percentage_cgpa">Percentage /CGPA <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('tlv_percentage_cgpa') is-invalid @enderror" placeholder="Percentage /CGPA" name="tlv_percentage_cgpa" id="tlv_percentage_cgpa" value="{{ old('tlv_percentage_cgpa')}}">
                                                            @error('tlv_percentage_cgpa')
                                                            <small class="invalid-feedback">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="tlv_passing_month">Month &amp; Year of Passing <span class="text-danger">*</span></label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <select class="form-control @error('tlv_passing_month') is-invalid @enderror" id="tlv_passing_month" name="tlv_passing_month" value="{{ old('tlv_passing_month')}}">
                                                                        <option  value="">--Month--</option>
                                                                        <option @if(old('tlv_passing_month') == 'January') selected @endif value="January">January</option>
                                                                        <option @if(old('tlv_passing_month') == 'February') selected @endif value="February">February </option>
                                                                        <option @if(old('tlv_passing_month') == 'March') selected @endif value="March">March</option>
                                                                        <option @if(old('tlv_passing_month') == 'April') selected @endif value="April">April </option>
                                                                        <option @if(old('tlv_passing_month') == 'May') selected @endif value="May">May </option>
                                                                        <option @if(old('tlv_passing_month') == 'June') selected @endif value="June">June </option>
                                                                        <option @if(old('tlv_passing_month') == 'July') selected @endif value="July">July </option>
                                                                        <option @if(old('tlv_passing_month') == 'August') selected @endif value="August">August </option>
                                                                        <option @if(old('tlv_passing_month') == 'September') selected @endif value="September">September </option>
                                                                        <option @if(old('tlv_passing_month') == 'October') selected @endif value="October">October </option>
                                                                        <option @if(old('tlv_passing_month') == 'November') selected @endif value="November">November </option>
                                                                        <option @if(old('tlv_passing_month') == 'December') selected @endif value="December">December </option>
                                                                    </select>
                                                                    @error('tlv_passing_month')
                                                                    <small class="invalid-feedback">This field is required!</small>
                                                                    @enderror
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <select class="form-control @error('tlv_passing_year') is-invalid @enderror" id="tlv_passing_year" name="tlv_passing_year" >
                                                                        <option  value="">--Year--</option>
                                                                        <option  @if(old('tlv_passing_year') == '2025') selected @endif value="2025">2025</option>
                                                                        <option  @if(old('tlv_passing_year') == '2024') selected @endif value="2024">2024</option>
                                                                        <option  @if(old('tlv_passing_year') == '2023') selected @endif value="2023">2023</option>
                                                                        <option  @if(old('tlv_passing_year') == '2022') selected @endif value="2022">2022</option>
                                                                        <option  @if(old('tlv_passing_year') == '2021') selected @endif value="2021">2021</option>
                                                                        <option  @if(old('tlv_passing_year') == '2020') selected @endif value="2020">2020</option>
                                                                        <option  @if(old('tlv_passing_year') == '2019') selected @endif value="2019">2019</option>
                                                                        <option  @if(old('tlv_passing_year') == '2018') selected @endif value="2018">2018</option>
                                                                        <option  @if(old('tlv_passing_year') == '2017') selected @endif value="2017">2017</option>
                                                                        <option  @if(old('tlv_passing_year') == '2016') selected @endif value="2016">2016</option>
                                                                        <option  @if(old('tlv_passing_year') == '2015') selected @endif value="2015">2015</option>
                                                                        <option  @if(old('tlv_passing_year') == '2014') selected @endif value="2014">2014</option>
                                                                        <option  @if(old('tlv_passing_year') == '2013') selected @endif value="2013">2013</option>
                                                                        <option  @if(old('tlv_passing_year') == '2012') selected @endif value="2012">2012</option>
                                                                        <option  @if(old('tlv_passing_year') == '2012') selected @endif value="2011">2011</option>
                                                                        <option  @if(old('tlv_passing_year') == '2010') selected @endif value="2010">2010</option>
                                                                        <option  @if(old('tlv_passing_year') == '2009') selected @endif value="2009">2009</option>
                                                                        <option  @if(old('tlv_passing_year') == '2008') selected @endif  value="2008">2008</option>
                                                                        <option  @if(old('tlv_passing_year') == '2007') selected @endif  value="2007">2007</option>
                                                                        <option  @if(old('tlv_passing_year') == '2006') selected @endif  value="2006">2006</option>
                                                                        <option  @if(old('tlv_passing_year') == '2005') selected @endif  value="2005">2005</option>
                                                                        <option  @if(old('tlv_passing_year') == '2004') selected @endif  value="2004">2004</option>
                                                                        <option  @if(old('tlv_passing_year') == '2003') selected @endif  value="2003">2003</option>
                                                                        <option  @if(old('tlv_passing_year') == '2002') selected @endif  value="2002">2002</option>
                                                                        <option  @if(old('tlv_passing_year') == '2001') selected @endif  value="2001">2001</option>
                                                                        <option  @if(old('tlv_passing_year') == '2000') selected @endif  value="2000">2000</option>
                                                                        <option  @if(old('tlv_passing_year') == '1999') selected @endif value="1999">1999</option>
                                                                        <option  @if(old('tlv_passing_year') == '1998') selected @endif value="1998">1998</option>
                                                                        <option  @if(old('tlv_passing_year') == '1997') selected @endif value="1997">1997</option>
                                                                        <option  @if(old('tlv_passing_year') == '1996') selected @endif value="1996">1996</option>
                                                                        <option  @if(old('tlv_passing_year') == '1995') selected @endif value="1995">1995</option>
                                                                    </select>
                                                                    @error('tlv_passing_year')
                                                                    <small class="invalid-feedback">This field is required!</small>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="tlv_medium_of_instruction">Medium of Instruction <span class="text-danger">*</span></label>
                                                            <select class="form-control @error('tlv_medium_of_instruction') is-invalid @enderror" id="tlv_medium_of_instruction" name="tlv_medium_of_instruction" value="{{ old('tlv_medium_of_instruction')}}">
                                                                <option value="">--select--</option>
                                                                <option @if(old('tlv_medium_of_instruction') == 'English') selected @endif  value="English">English</option>
                                                                <option @if(old('tlv_medium_of_instruction') == 'Regional Language') selected @endif  value="Regional Language">Regional Language</option>
                                                            </select>
                                                            @error('tlv_medium_of_instruction')
                                                            <small class="invalid-feedback" style="display: block">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="tlv_passed_in_single_attempt">Passed in Single Attempt <span class="text-danger">*</span></label>
                                                            <br>{{ old('tlv_passed_in_single_attempt')}}
                                                            <input type="radio" @if(old('tlv_passed_in_single_attempt') == 'Yes') checked @endif name="tlv_passed_in_single_attempt" id="tlv_Yes" class="@error('tlv_passed_in_single_attempt') is-invalid @enderror" value="Yes"> <label for="tlv_Yes"> Yes</label>
                                                            <input type="radio" @if(old('tlv_passed_in_single_attempt') == 'No') checked @endif name="tlv_passed_in_single_attempt" id="tlv_No" class="@error('tlv_passed_in_single_attempt') is-invalid @enderror" value="No"> <label for="tlv_No"> No</label>
                                                            @error('tlv_passed_in_single_attempt')
                                                            <small class="invalid-feedback" style="display: block">This field is required!</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row custom_bg">
                                                    <div class="col-md-12">
                                                        <div class="head_custom">
                                                            <h5>Under Graduate Degree</h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="degree">Degree <span class="text-danger">*</span></label>
                                                            <select class="form-control @error('degree') is-invalid @enderror" id="degree" name="degree" value="{{ old('degree')}}">
                                                                <option value="">--Select One--</option>
                                                                <option @if(old('degree') == 'B.COM') selected @endif value="B.COM">B.COM</option>
                                                                <option @if(old('degree') == 'BBA') selected @endif value="BBA">BBA</option>
                                                                <option @if(old('degree') == 'BCA') selected @endif value="BCA">BCA</option>
                                                                <option @if(old('degree') == 'B.E') selected @endif value="B.E">B.E</option>
                                                                <option @if(old('degree') == 'B.Tech') selected @endif value="B.Tech">B.Tech</option>
                                                                <option @if(old('degree') == 'BBM') selected @endif value="BBM">BBM</option>
                                                                <option @if(old('degree') == 'B.Sc') selected @endif value="B.Sc">B.Sc</option>
                                                                <option @if(old('degree') == 'BA') selected @endif value="BA">BA</option>
                                                                <option @if(old('degree') == 'Others') selected @endif value="Others">Others</option>
                                                            </select>
                                                            @error('degree')
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('degree') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="blankDiv" style="display:block"></div>
                                                    <div class="col-md-4" id="specializationDiv" style="display:none">
                                                        <div class="form-group">
                                                            <label for="specialization">Specialization<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('specialization') is-invalid @enderror" placeholder="Specialization" name="specialization" id="specialization" value="{{ old('specialization')}}">
                                                            @error('specialization')
                                                            <small class="invalid-feedback">{{ $errors->first('specialization') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4" id="dept_branch_subjectDiv" style="display:none">
                                                        <div class="form-group">
                                                            <label for="dept_branch_subject">Department/Branch/Subject<span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control error_icon @error('dept_branch_subject') is-invalid @enderror" placeholder="Department/Branch/Subject" name="dept_branch_subject" id="dept_branch_subject" value="{{ old('dept_branch_subject')}}">
                                                            @error('dept_branch_subject')
                                                            <small class="invalid-feedback">{{ $errors->first('dept_branch_subject') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="name_of_college">Name of the College <span class="text-danger">*</span></label>
                                                                    <input type="text" class="form-control error_icon @error('name_of_college') is-invalid @enderror" placeholder="Percentage /CGPA" name="name_of_college" id="name_of_college" value="{{ old('name_of_college')}}">
                                                                    @error('name_of_college')
                                                                    <small class="invalid-feedback">{{ $errors->first('name_of_college') }}</small>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="name_of_university">Name of the University <span class="text-danger">*</span></label>
                                                                    <input type="text" class="form-control error_icon @error('name_of_university') is-invalid @enderror" placeholder="Name of the University" name="name_of_university" id="name_of_university" value="{{ old('name_of_university')}}">
                                                                    @error('name_of_university')
                                                                    <small class="invalid-feedback">{{ $errors->first('name_of_university') }}</small>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 px-0">
                                                                <div class="form-group">
                                                                    <label for="overall_cgp">Overall %/CGPA <small>(up to pre-final/final Sem)</small><span class="text-danger">*</span></label>
                                                                    <input type="text" class="form-control error_icon @error('overall_cgp') is-invalid @enderror" placeholder="Overall %/CGPA" name="overall_cgp" id="overall_cgp" value="{{ old('overall_cgp')}}">
                                                                    @error('overall_cgp')
                                                                    <small class="invalid-feedback">{{ $errors->first('overall_cgp') }}</small>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="degree_passing_month">Month &amp; Year of Passing </label>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <select class="form-control @error('degree_passing_month') is-invalid @enderror" id="degree_passing_month" name="degree_passing_month" value="{{ old('degree_passing_month')}}">
                                                                        <option  value="">--Month--</option>
                                                                        <option @if(old('degree_passing_month') == 'January') selected @endif value="January">January</option>
                                                                        <option @if(old('degree_passing_month') == 'February') selected @endif value="February">February </option>
                                                                        <option @if(old('degree_passing_month') == 'March') selected @endif value="March">March</option>
                                                                        <option @if(old('degree_passing_month') == 'April') selected @endif value="April">April </option>
                                                                        <option @if(old('degree_passing_month') == 'May') selected @endif value="May">May </option>
                                                                        <option @if(old('degree_passing_month') == 'June') selected @endif value="June">June </option>
                                                                        <option @if(old('degree_passing_month') == 'July') selected @endif value="July">July </option>
                                                                        <option @if(old('degree_passing_month') == 'August') selected @endif value="August">August </option>
                                                                        <option @if(old('degree_passing_month') == 'September') selected @endif value="September">September </option>
                                                                        <option @if(old('degree_passing_month') == 'October') selected @endif value="October">October </option>
                                                                        <option @if(old('degree_passing_month') == 'November') selected @endif value="November">November </option>
                                                                        <option @if(old('degree_passing_month') == 'December') selected @endif value="December">December </option>
                                                                    </select>
                                                                    @error('degree_passing_month')
                                                                    <small class="invalid-feedback" style="display: block">{{ $errors->first('degree_passing_month') }}</small>
                                                                    @enderror
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <select class="form-control @error('degree_passing_year') is-invalid @enderror" id="degree_passing_year" name="degree_passing_year" >
                                                                        <option  value="">--Year--</option>
                                                                        <option  @if(old('degree_passing_year') == '2025') selected @endif value="2025">2025</option>
                                                                        <option  @if(old('degree_passing_year') == '2024') selected @endif value="2024">2024</option>
                                                                        <option  @if(old('degree_passing_year') == '2023') selected @endif value="2023">2023</option>
                                                                        <option  @if(old('degree_passing_year') == '2022') selected @endif value="2022">2022</option>
                                                                        <option  @if(old('degree_passing_year') == '2021') selected @endif value="2021">2021</option>
                                                                        <option  @if(old('degree_passing_year') == '2020') selected @endif value="2020">2020</option>
                                                                        <option  @if(old('degree_passing_year') == '2019') selected @endif value="2019">2019</option>
                                                                        <option  @if(old('degree_passing_year') == '2018') selected @endif value="2018">2018</option>
                                                                        <option  @if(old('degree_passing_year') == '2017') selected @endif value="2017">2017</option>
                                                                        <option  @if(old('degree_passing_year') == '2016') selected @endif value="2016">2016</option>
                                                                        <option  @if(old('degree_passing_year') == '2015') selected @endif value="2015">2015</option>
                                                                        <option  @if(old('degree_passing_year') == '2014') selected @endif value="2014">2014</option>
                                                                        <option  @if(old('degree_passing_year') == '2013') selected @endif value="2013">2013</option>
                                                                        <option  @if(old('degree_passing_year') == '2012') selected @endif value="2012">2012</option>
                                                                        <option  @if(old('degree_passing_year') == '2012') selected @endif value="2011">2011</option>
                                                                        <option  @if(old('degree_passing_year') == '2010') selected @endif value="2010">2010</option>
                                                                        <option  @if(old('degree_passing_year') == '2009') selected @endif value="2009">2009</option>
                                                                        <option  @if(old('degree_passing_year') == '2008') selected @endif  value="2008">2008</option>
                                                                        <option  @if(old('degree_passing_year') == '2007') selected @endif  value="2007">2007</option>
                                                                        <option  @if(old('degree_passing_year') == '2006') selected @endif  value="2006">2006</option>
                                                                        <option  @if(old('degree_passing_year') == '2005') selected @endif  value="2005">2005</option>
                                                                        <option  @if(old('degree_passing_year') == '2004') selected @endif  value="2004">2004</option>
                                                                        <option  @if(old('degree_passing_year') == '2003') selected @endif  value="2003">2003</option>
                                                                        <option  @if(old('degree_passing_year') == '2002') selected @endif  value="2002">2002</option>
                                                                        <option  @if(old('degree_passing_year') == '2001') selected @endif  value="2001">2001</option>
                                                                        <option  @if(old('degree_passing_year') == '2000') selected @endif  value="2000">2000</option>
                                                                        <option  @if(old('degree_passing_year') == '1999') selected @endif value="1999">1999</option>
                                                                        <option  @if(old('degree_passing_year') == '1998') selected @endif value="1998">1998</option>
                                                                        <option  @if(old('degree_passing_year') == '1997') selected @endif value="1997">1997</option>
                                                                        <option  @if(old('degree_passing_year') == '1996') selected @endif value="1996">1996</option>
                                                                        <option  @if(old('degree_passing_year') == '1995') selected @endif value="1995">1995</option>
                                                                    </select>
                                                                    @error('degree_passing_year')
                                                                    <small class="invalid-feedback" style="display: block">{{ $errors->first('degree_passing_year') }}</small>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="degree_medium_of_instruction">Medium of Instruction <span class="text-danger">*</span></label>
                                                            <select class="form-control @error('degree_medium_of_instruction') is-invalid @enderror" id="degree_medium_of_instruction" name="degree_medium_of_instruction" value="{{ old('degree_medium_of_instruction')}}">
                                                                <option value="">--select--</option>
                                                                <option @if(old('degree_medium_of_instruction') == 'English') selected @endif  value="English">English</option>
                                                                <option @if(old('degree_medium_of_instruction') == 'Regional Language') selected @endif  value="Regional Language">Regional Language</option>
                                                            </select>
                                                            @error('degree_medium_of_instruction')
                                                            <small class="invalid-feedback" style="display: block">{{ $errors->first('degree_medium_of_instruction') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="head_custom pt-3">
                                                            <h5 class="text-dark">Other Educational Information</h5>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="other_educational_information">No. of Courses not cleared in First Attempt</label>
                                                            <input type="text" class="form-control" name="other_educational_information" id="other_educational_information" value="{{ old('other_educational_information')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="ug_time_of_applying">Standing arrears in UG at the time of applying</label>
                                                            <input type="text" class="form-control"  name="ug_time_of_applying" id="ug_time_of_applying" value="{{ old('ug_time_of_applying')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Academic Details card end -->
                                        <!-- 11th card start -->
                                        <!-- <div class="card">
                                            <div class="card-body label_css">
                                            	<div class="row">
                                            		<div class="col-md-12">
                                            			<div class="card-head">
                                            				<h6 class="badge badge-pill pil_custom badge-danger">11. Admission Test Result</h6>
                                            			</div>
                                            		</div>
                                                                                 </div>
                                                                                 
                                            	<div class="row">
                                            		<div class="col-md-4">
                                            			<div class="form-group">
                                            				<label for="select_test">Select Test</label>
                                            				<select class="form-control" id="select_test" name="select_test">
                                            					<option  value="">--select--</option>
                                                                                                 <option @if(old('select_test') == 'CAT') selected @endif value="CAT">CAT</option>
                                                                                                 <option @if(old('select_test') == 'CMAT') selected @endif value="CMAT">CMAT</option>
                                                                                                 <option @if(old('select_test') == 'GMAT') selected @endif value="GMAT">GMAT</option>
                                                                                                 <option @if(old('select_test') == 'MAT-DEC') selected @endif value="MAT-DEC">MAT-DEC</option>
                                                                                                 <option @if(old('select_test') == 'MAT-FEB') selected @endif value="MAT-FEB">MAT-FEB</option>
                                                                                                 <option @if(old('select_test') == 'MAT-MAY') selected @endif value="MAT-MAY">MAT-MAY</option>
                                                                                                 <option @if(old('select_test') == 'MAT-SEP') selected @endif value="MAT-SEP">MAT-SEP</option>
                                                                                                 <option @if(old('select_test') == 'OTHERS') selected @endif value="OTHERS">OTHERS</option>
                                                                                                 <option @if(old('select_test') == 'TANCET') selected @endif value="TANCET">TANCET</option>
                                                                                                 <option @if(old('select_test') == 'XAT') selected @endif value="XAT">XAT</option>
                                                                                             </select>
                                                                                             @error('degree_medium_of_instruction')
                                            				<small class="invalid-feedback" style="display: block">{{ $errors->first('select_test') }}</small>
                                            				@enderror
                                            			</div>
                                            		</div>
                                            		<div class="col-md-6">
                                            			<div class="form-group">
                                            				<label for="date_of_test">Date of Test </label>
                                            				<input type="text" class="form-control" placeholder="Date of Birth " name="date_of_test" id="datepicker2" value="{{ old('date_of_test')}}">
                                            			</div>
                                            		</div>
                                            		<div class="col-md-6">
                                            			<div class="form-group">
                                            				<label for="registration_number">Registration Number</label>
                                            				<input type="text" class="form-control" placeholder="Registration Number" name="registration_number" id="registration_number" value="{{ old('registration_number')}}">
                                            			</div>
                                            		</div>
                                            	</div>
                                            	<div class="row">
                                            		<div class="col-md-4">
                                            			<div class="form-group">
                                            				<label for="overall_score">Overall Score</label>
                                            				<input type="text" class="form-control" placeholder="Overall Score" name="overall_score" id="overall_score" value="{{ old('overall_score')}}">
                                            			</div>
                                            		</div>
                                            		<div class="col-md-4">
                                            			<div class="form-group">
                                            				<label for="overall_percentile">Overall Percentile</label>
                                            				<input type="text" class="form-control" placeholder="Overall Percentile" name="overall_percentile" id="overall_percentile" value="{{ old('overall_percentile')}}">
                                            			</div>
                                            		</div>
                                            		
                                            	</div>
                                            </div>
                                            </div> -->
                                        <!-- 11th card end -->
                                        <!-- 12th card start -->
                                        <div class="card">
                                            <div class="card-body label_css">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">5. Program Selection</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program">Select Programme<span class="text-danger">*</span></label>
                                                            <select class="form-control @error('program') is-invalid @enderror" name="program" id="program" value="{{ old('program')}}">
                                                                <option value="">--select--</option>
                                                                <option @if(old('program') == 'M.Sc.') selected @endif value="M.Sc.">M.Sc.</option>
                                                                <option @if(old('program') == 'M.C.A') selected @endif value="M.C.A">M.C.A</option>
                                                                <option @if(old('program') == 'M.Com') selected @endif value="M.Com">M.Com</option>
                                                                <option @if(old('program') == 'M.B.A') selected @endif value="M.B.A">M.B.A</option>
                                                                <option @if(old('program') == 'M.A.') selected @endif value="M.A.">M.A.</option>
                                                            </select>
                                                            @error('program')
                                                            <small class="invalid-feedback" style="display: block">{{ $errors->first('program') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Specific programme <span class="text-danger">*</span></label>
                                                            <select class="form-control @error('specific_programme') is-invalid @enderror" name="specific_programme" id="specific_programme" value="{{ old('specific_programme')}}">
                                                            @if(old('specific_programme'))
                                                                <option value="{{ old('specific_programme') }}">{{ old('specific_programme') }} </option>
                                                            @else
                                                                <option value="">--select--</option>
                                                            @endif
                                                            </select>
                                                            @error('specific_programme')
                                                            <small class="invalid-feedback" style="display:block">{{ $errors->first('specific_programme') }}</small>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="head_custom">
                                                            <h5>Fees: INR 300/-</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 12th card end -->
                                        <button type="submit" class="btn btn-danger btn-fill ">Submit</button> 
                                        <button type="reset" class="btn btn-secondary ml-2 btn-fill ">Reset</button>
                                        <div class="clearfix">                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection