@extends('layouts.admin')
@section('content')
<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">List of application</h4>
            
        </div>
        <div class="card-body table-full-width table-responsive">
            <form action="{{ url('applications/filter')}}" method="GET">
                <div class="row" style="padding: 15px">
                    <div class="col-md-3">
                        <input class="form-control" type="text" name="q" value="{{ app('request')->input('q')
}}" placeholder="Search by anything...">
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" name="p" id="program">
                            <option value="">--program--</option>
                            <option @if(app('request')->input('p') == 'M.Sc.') selected @endif value="M.Sc.">M.Sc.</option>
                            <option @if(app('request')->input('p') == 'M.C.A') selected @endif value="M.C.A">M.C.A</option>
                            <option @if(app('request')->input('p') == 'M.Com') selected @endif value="M.Com">M.Com</option>
                            <option @if(app('request')->input('p') == 'M.B.A') selected @endif value="M.B.A">M.B.A</option>
                            <option @if(app('request')->input('p') == 'M.A.') selected @endif value="M.A.">M.A.</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" name="sp" id="specific_programme">
                        @if(app('request')->input('sp'))
                            <option value="{{ app('request')->input('sp') }}">{{ app('request')->input('sp') }} </option>
                        @else
                            <option value="">--select--</option>
                        @endif
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control" name="s" id="status">
                            <option value="">--status--</option>
                            <option @if(app('request')->input('s') == 'Pending') selected @endif value="Pending">Pending</option>
                            <option @if(app('request')->input('s') == 'Accepted') selected @endif value="Accepted">Accepted</option>
                            <option @if(app('request')->input('s') == 'Rejected') selected @endif value="Rejected">Rejected</option>
                            <option @if(app('request')->input('s') == 'Fee pending') selected @endif value="Fee paid">Fee Pending</option>
                            <option @if(app('request')->input('s') == 'Fee paid') selected @endif value="Fee paid">Fee Paid</option>
                            <option @if(app('request')->input('s') == 'Verify pending') selected @endif value="Verify pending">Verify Pending</option>
                            <option @if(app('request')->input('s') == 'Verified') selected @endif value="Verified">Verified</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-sm btn-info btn-fill" type="submit">Search</button>
                    </div>
                </div>
            </form>
            <table class="table">
           
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Mobile No</th>
                        <th>Email</th>
                        <th>Program</th>
                        <th>Specific Program</th>
                        <th>Payment Id</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @php $sn = $all->firstItem() @endphp
                    @foreach($all as $app)
                    <tr style="background: {{ $app->bg_color }}">
                        <td> {{ $sn++ }}</td>
                        <td>{{$app->first_name}} {{$app->last_name}}</td>
                        <td>{{$app->mobile_number}}</td>
                        <td>{{$app->email_id}} </td>
                        <td>{{$app->program}}</td>
                        <td>{{$app->specific_programme}}</td>
                        <td>{{ substr($app->razorpay_payment_id, 4)}}</td>
                        <td>{{$app->application_status}}</td>
                        <td><a class="btn btn-info btn-sm" href="{{ url('applications',$app->student_application_id )}}">View</a></td>
                    </tr>
                    @endforeach
                </tbody>
                
            </table>
            <h5 class="card-header">Showing results {{ $all->lastItem() }} of {{ $all->total() }}</h5>
            <div class="row">
                <div class="col-lg-1 col-md-1 col-centered">
                    {{ $all->withQueryString()->links() }}
                    <a href="{{ url('applications/download?'.$query)}}" class="btn btn-primary btn-fill">Download</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection