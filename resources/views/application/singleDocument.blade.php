@extends('layouts.students')
@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title text-center">Documents Verification</h4>
						 @empty($document)
                            <span class="text-danger text-center">No record found</span>
                        @endempty
					</div>
					<div class="card-body label_css">
						<form action="{{ url('application-status')}}" method="GET">
							<div class="container-fluid">
								<div class="row justify-content-center">
									<div class="col-md-10">
                                        @isset($status)
										<div class="card">
                                            <div class="card-body label_css">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">Payment Information</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program">Paid <span class="text-danger">*</span></label>
                                                            <p class="form-control" >INR {{ $status->amount }} /-</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Application Status <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->application_status }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Program <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->program }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Specific Program <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->specific_programme }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger"> Personal Details</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="first_name">Name <span class="text-danger">*</span></label>
                                                            <p class="uppercase form-control" >{{ $status->first_name }}  {{ $status->last_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                            <label for="first_name">Father Name <span class="text-danger">*</span></label>
                                                            <p class="uppercase form-control" >{{ $status->father_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="last_name">Mother Name <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->mother_name }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="gender">Gender <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->gender }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                        <label for="email_id">Email ID <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->email_id }}</p>
                                                        </div>
                                                    </div>
													@if($status->application_status  == 'Accepted')
														<div class="container">
															<div class="col-md-3 pull-right">
																<a href="{{ url('upload-documents?mobile_number='.$_GET['mobile_number'].'&date_of_birth='.$_GET['date_of_birth'])}}" class="btn btn-warning btn-fill">Upload Documents</a>   
															</div>
														</div>
                                                	@endif
                                                </div>
                                               
                                                <div class="clearfix"></div>
                                                
                                                  <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">photo</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->photo)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">transfer certificate</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->transfer_certificate)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">conduct_certificate</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->conduct_certificate)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">mark_sheet</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->mark_sheet)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">provisional_degree_certificate</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->provisional_degree_certificate)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">aadhar_card_front</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->aadhar_card_front)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">aadhar_card_back</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->aadhar_card_back)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">parent_aadhar_card_fronte</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->parent_aadhar_card_front)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                 <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">parent_aadhar_card_back</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                           <img src="{{ Storage::url('app/public/documents/'.$document->student_application_id.'/'.$document->parent_aadhar_card_back)}}" class="img-fluid img-thumbnail " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        @endisset
										<div class="clearfix">                
										</div>
										
										 @isset($document)
                                            <a href="{{ url('/applications/'.$document->student_application_id.'/document/verify')}}" class="btn btn-warning btn-fill ">Verify Documents</a> 
                                        @endisset
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection