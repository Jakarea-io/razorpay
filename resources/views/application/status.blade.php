@extends('layouts.students')
@section('content')
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<div class="card">
					<div class="card-header">
						<h4 class="card-title text-center">Check Application Status</h4>
					</div>
					<div class="card-body label_css">
						<form action="{{ url('application-status')}}" method="GET">
							<div class="container-fluid">
								<div class="row justify-content-center">
									<div class="col-md-10">
										<div class="card">
											<div class="card-body label_css">
												<div class="row">
                                                    <div class="col-md-6">
														<div class="form-group">
															<label for="mobile_number">Mobile Number <span class="text-danger">*</span></label>
															<div class="input-group">
																<span class="input-group-addon">+91</span>	
																<input type="text" required class="form-control error_icon @error('mobile_number') is-invalid @enderror" placeholder="Mobile Number" name="mobile_number" id="mobile_number">
															</div>
														</div>
													</div>
                                                    <div class="col-md-6">
														<div class="form-group">
															<label for="date_of_birth">Date of Birth <span class="text-danger">*</span></label>
															<input type="date" required class="form-control @error('date_of_birth') is-invalid @enderror" placeholder="Date of Birth " name="date_of_birth">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-offset-6 col-md-6 pull-right">
                                                    <button type="submit" class="btn btn-danger btn-fill">Submit</button>   
                                                    </div>
                                                </div>
											</div>
										</div>
										<!-- second card end -->
										
										<!-- Academic Details card start -->
                                        @isset($status)
										<div class="card">
                                            <div class="card-body label_css">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">Payment Information</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program">Paid <span class="text-danger">*</span></label>
                                                            <p class="form-control" >INR {{ $status->amount }} /-</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Application Status <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->application_status }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Program <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->program }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="program_group">Specific Program <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->specific_programme }}</p>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                                

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card-head">
                                                            <h6 class="badge badge-pill pil_custom badge-danger">1. Personal Details</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="first_name">Name <span class="text-danger">*</span></label>
                                                            <p class="uppercase form-control" >{{ $status->first_name }}  {{ $status->last_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                            <label for="first_name">Father Name <span class="text-danger">*</span></label>
                                                            <p class="uppercase form-control" >{{ $status->father_name }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="last_name">Mother Name <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->mother_name }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="gender">Gender <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->gender }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                        <label for="email_id">Email ID <span class="text-danger">*</span></label>
                                                            <p class="form-control" >{{ $status->email_id }}</p>
                                                        </div>
                                                    </div>
													@if($status->application_status  == 'Accepted')
														<div class="container">
															<div class="col-md-3 pull-right">
																<a href="{{ url('upload-documents?mobile_number='.$_GET['mobile_number'].'&date_of_birth='.$_GET['date_of_birth'])}}" class="btn btn-warning btn-fill">Upload Documents</a>   
															</div>
														</div>
                                                    @elseif($status->application_status == 'Fee pending' || $status->application_status == 'Verified')
                                                        <div class="container">
															<div class="col-md-3 pull-right">
																<a href="{{ url('tuition-fee?mobile_number='.$_GET['mobile_number'].'&date_of_birth='.$_GET['date_of_birth'])}}" class="btn btn-warning btn-fill">Pay Now</a>   
															</div>
														</div>
                                                	@endif
                                                </div>
                                               
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>		
                                        @endisset
                                        @empty($status)
                                            @php if(isset($_GET['mobile_number']) && isset($_GET['date_of_birth'])) 
                                                echo '<span class="text-danger">No record found</span>'
                                            @endphp
                                        @endempty
										<div class="clearfix">                
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection