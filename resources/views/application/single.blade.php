@extends('layouts.admin')
@section('content')
<div class="col-md-12">
	<div class="card strpied-tabled-with-hover">
		<div class="card-header ">
			<h4 class="card-title">Applicant Details</h4>
            <a href="{{ url('/applications/'.$application->student_application_id.'/pdf') }}" class="btn btn-info btn-fill pull-right">download as pdf</a>
		</div>
        
        <div class="card-body label_css">
            <form>
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body label_css">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-head">
                                                <h6 class="badge badge-pill pil_custom badge-danger">Payment Information</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program">Paid <span class="text-danger">*</span></label>
                                                <p class="form-control" >INR {{ $application->amount }} /-</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program_group">Application Status <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->application_status ??'pending'}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program_group">Payment Id <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->razorpay_payment_id }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program_group">Order Id <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->razorpay_order_id }}</p>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-head">
                                                <h6 class="badge badge-pill pil_custom badge-danger">1. Personal Details</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="first_name">first name <span class="text-danger">*</span></label>
                                                <p class="uppercase form-control" >{{ $application->first_name }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                        <div class="form-group">
                                                <label for="first_name">Middle name <span class="text-danger">*</span></label>
                                                <p class="uppercase form-control" >{{ $application->middle_name }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="last_name">Last name <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->last_name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="gender">Gender <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->gender }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="date_of_birth">Date of Birth <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->date_of_birth }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email_id">Email ID <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->email_id }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="alt_email_id">Alternate Email ID </label>
                                                <p class="form-control" >{{ $application->alt_email_id }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="mobile_number">Mobile Number <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->mobile_number? '+91 '.$application->mobile_number : '' }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="mobile_number">Alternate Mobile Number </label>
                                                <p class="form-control" >{{ $application->alt_mobile_number? '+91 '.$application->alt_mobile_number : '' }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="student_aadhar_number">Aadhar Number <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->student_aadhar_number }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="parent_aadhar_number">Parent Aadhar Number <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->parent_aadhar_number }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="nationality">Nationality </label>
                                                <p class="form-control" >{{ $application->nationality }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="blood_group">Blood Group</label>
                                                <p class="form-control" >{{ $application->blood_group }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="religion">Religion</label>
                                                <p class="form-control" >{{ $application->religion }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="community">Community</label>
                                                <p class="form-control" >{{ $application->community }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <!-- first card end -->
                            <!-- second card start -->
                            <div class="card">
                                <div class="card-body label_css">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-head">
                                                <h6 class="badge badge-pill pil_custom badge-danger">2. Contact Information</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="address">Address<span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->address }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="country">Country</label>
                                                <p class="form-control" >{{ $application->country }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="state">State <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->state }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="city">City <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->city }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="pin_code">Pin Code<span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->pin_code }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- second card end -->
                            <!-- third card start -->
                            <div class="card">
                                <div class="card-body label_css">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-head">
                                                <h6 class="badge badge-pill pil_custom badge-danger">3. Parents / Guardian Details</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="father_name">Father Name <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->father_name }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="father_occupation">Father Occupation <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->father_occupation }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="father_mobile_number">Father Mobile Number</label>
                                                <p class="form-control" >{{ $application->father_mobile_number? '+91 '.$application->father_mobile_number : '' }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="mother_name">Mother Name <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->mother_name }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="father_occupation">Mother Occupation </label>
                                                <p class="form-control" >{{ $application->mother_occupation }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="mother_mobile_number">Mother Mobile Number</label>
                                                <p class="form-control" >{{ $application->mother_mobile_number? '+91 '.$application->mother_mobile_number : '' }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="mother_name">Guardian Name </label>
                                                <p class="form-control" >{{ $application->guardian_name }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="father_occupation">Guardian Occupation </label>
                                                <p class="form-control" >{{ $application->guardian_occupation }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="guardian_mobile_number">Guardian Mobile Number</label>
                                                <p class="form-control" >{{ $application->guardian_mobile_number? '+91'.$application->guardian_mobile_number : '' }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- third card end -->
                            <!-- Academic Details card start -->
                            <div class="card">
                                <div class="card-body label_css">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-head">
                                                <h6 class="badge badge-pill pil_custom badge-danger">4. Academic Details</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 10th Standard start -->
                                    <div class="row custom_bg">
                                        <div class="col-md-12">
                                            <div class="head_custom">
                                                <h5>10th Standard</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="th_board">Board <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->th_board }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4" id="otherTh">
                                            <div class="form-group">
                                                <label for="th_other_board">Please Specify Other Board  <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->th_other_board }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="th_name_of_school">Name of the School  <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->th_name_of_school }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="th_percentage_cgpa">Percentage /CGPA <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->th_percentage_cgpa }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="th_passing_month">Month &amp; Year of Passing <span class="text-danger">*</span></label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <p class="form-control" >{{ $application->th_passing_month }}</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <p class="form-control" >{{ $application->th_passing_year }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="th_medium_of_instruction">Medium of Instruction <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->th_medium_of_instruction }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="th_passed_in_single_attempt">Passed in Single Attempt <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->th_passed_in_single_attempt }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 10th Standard end -->
                                    <!-- 12th Standard / Diploma start -->
                                    <div class="row custom_bg">
                                        <div class="col-md-12">
                                            <div class="head_custom">
                                                <h5>12th Standard / Diploma</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tlv_board">Board <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->tlv_board }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4" id="otherTlv">
                                            <div class="form-group">
                                                <label for="tlv_other_board">Please Specify Other Board  <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->tlv_other_board }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tlv_name_of_school">Name of the School  <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->tlv_name_of_school }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tlv_percentage_cgpa">Percentage /CGPA <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->tlv_percentage_cgpa }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tlv_passing_month">Month &amp; Year of Passing <span class="text-danger">*</span></label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <p class="form-control" >{{ $application->tlv_passing_month }}</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <p class="form-control" >{{ $application->tlv_passing_year }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tlv_medium_of_instruction">Medium of Instruction <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->tlv_medium_of_instruction }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="tlv_passed_in_single_attempt">Passed in Single Attempt <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->tlv_passed_in_single_attempt }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row custom_bg">
                                        <div class="col-md-12">
                                            <div class="head_custom">
                                                <h5>Under Graduate Degree</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="degree">Degree <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->degree }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4" id="blankDiv" style="display:block"></div>
                                        <div class="col-md-4" id="specializationDiv">
                                            <div class="form-group">
                                                <label for="specialization">Specialization<span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->specialization }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4" id="dept_branch_subjectDiv">
                                            <div class="form-group">
                                                <label for="dept_branch_subject">Department/Branch/Subject<span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->dept_branch_subject }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="name_of_college">Name of the College <span class="text-danger">*</span></label>
                                                        <p class="form-control" >{{ $application->name_of_college }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="name_of_university">Name of the University <span class="text-danger">*</span></label>
                                                        <p class="form-control" >{{ $application->name_of_university }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 px-0">
                                                    <div class="form-group">
                                                        <label for="overall_cgp">Overall %/CGPA <small>(up to pre-final/final Sem)</small><span class="text-danger">*</span></label>
                                                        <p class="form-control" >{{ $application->overall_cgp }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="degree_passing_month">Month &amp; Year of Passing </label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                    <p class="form-control" >{{ $application->degree_passing_month }}</p>
                                                    </div>
                                                    <div class="col-md-6">
                                                    <p class="form-control" >{{ $application->degree_passing_year }}</p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="degree_medium_of_instruction">Medium of Instruction <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->degree_medium_of_instruction }}</p>
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="head_custom pt-3">
                                                <h5 class="text-dark">Other Educational Information</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="other_educational_information">No. of Courses not cleared in First Attempt</label>
                                                <p class="form-control" >{{ $application->other_educational_information }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="ug_time_of_applying">Standing arrears in UG at the time of applying</label>
                                                <p class="form-control" >{{ $application->ug_time_of_applying }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="card">
                                <div class="card-body label_css">
                                    <div class="row">
                                        <div class="col-md-12">
                                        <img src="{{ url('storage/app/public/documents/'.$document->student_application_id.'/sidebar-1.jpg') }}" style="width:300px;height:300px;"></br>                  
                                            <div class="card-head">
                                            
                                                <h6 class="badge badge-pill pil_custom badge-danger">5. Program Selection</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program">Selected Programme<span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->program }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="program_group">Specific programme <span class="text-danger">*</span></label>
                                                <p class="form-control" >{{ $application->specific_programme }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="head_custom">
                                                <h5>Fees: INR 300/-</h5>
                                            </div>
                                        </div>
                                    </div>        
                                </div>
                            </div>
                            <!-- 12th card end -->
                            @if($application->application_status == 'Pending')
                                <a href="{{ url('/applications/'.$application->student_application_id.'/accept')}}" class="btn btn-success btn-fill ">Accept</a> 
                                <a href="{{ url('/applications/'.$application->student_application_id.'/reject')}}" class="btn btn-danger ml-2 btn-fill ">Reject</a>
                            @elseif( $application->application_status == 'Rejected')
                                <a href="{{ url('/applications/'.$application->student_application_id.'/accept')}}" class="btn btn-success btn-fill ">Accept</a> 
                            @elseif( $application->application_status == 'Fee paid')
                                <a href="{{ url('/applications/'.$application->student_application_id.'/verified')}}" class="btn btn-warning btn-fill pull-right">Payment Verified</a>
                            @endif

                            @if($document)
                            <a href="{{ url('/applications/'.$application->student_application_id.'/documents')}}" class="btn btn-success btn-fill ">Documents</a> 
                            @endif
                            <div class="clearfix">                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
	</div>
</div>
@endsection