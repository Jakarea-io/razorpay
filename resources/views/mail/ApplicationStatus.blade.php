<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Registration</title>
</head>
<body>
    <h1>{{ $details['subject']}}</h1>
    <p> Dear {{ $details['name']}}</p>
    <p>
        {{ $details['text']}} <a href="{{ $details['link'] }}">
    </p>
    <p> Thank you</p>
</body>
</html>