<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/x-icon" href="/assets/img/logo.png">
  
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{ config('app.name', 'J.J. College of Arts & Science') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <!-- date picker css -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/public/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ url('/public/assets/css/light-bootstrap-dashboard.css?v=2.0.0') }}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{ url('/public/assets/css/demo.css') }}" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">
        <div class="main-panel no-sidebar">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#pablo"> 
                    <img src="{{ url('/public/assets/img/logo.png') }}" style="height: 40px"/>
                    </a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="dropdown">
                                   
                                    <span class="d-lg-none">Dashboard</span>
                                </a>
                            </li>
                            
                           
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('online-application')}}">
                                    <span class="no-icon">Online Application</span>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('application-status')}}">
                                    <span class="no-icon">Application Status</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="row justify-content-center">
                <div class="col-md-10">
                    @if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif
                    @if(Session::has('success'))
                        <p class="alert alert-success">{{ Session::get('success') }}</p>
                    @endif
                    @if(Session::has('warning'))
                        <p class="alert alert-warning">{{ Session::get('warning') }}</p>
                    @endif
                </div>
            </div>

            @yield('content')
            <footer class="footer">
                <div class="container-fluid">
                    <nav>
                        <p class="copyright text-center">
                            <a href="http://kathir.biz/" target="_blank">Kathir Technologies</a> © 
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                             made with love 
                        </p>
                    </nav>
                </div>
            </footer>
        </div>
    </div>
   
</body>
<!--   Core JS Files   -->
<script src="{{ url('/public/assets/js/core/jquery.3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ url('/public/assets/js/core/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ url('/public/assets/js/core/bootstrap.min.js') }}" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="{{ ('/public/assets/js/plugins/bootstrap-switch.js') }}"></script>
<!--  Chartist Plugin  -->
<!-- date picker js -->
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="{{ url('/public/assets/js/plugins/chartist.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ url('public/assets/js/plugins/bootstrap-notify.js') }}"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="{{ url('public/assets/js/light-bootstrap-dashboard.js?v=2.0.0') }}" type="text/javascript"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ url('public/assets/js/demo.js') }}"></script>
<script src="{{ url('public/assets/js/city.js') }}"></script>
<script>
    $(function($) {
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
        
        if(document.getElementById('th_board').value == "Other"){
            $('#otherTh').css('display','block')
        }
        $('.getOtherTh').on('change', function() {
           if(this.value == 'Other'){
               $('#otherTh').css('display','block')
           }else{
            $('#otherTh').css('display','none')
           }
        });
        if(document.getElementById('tlv_board').value == "Other"){
            $('#otherTlv').css('display','block')
        }else{
            $('#otherTh').css('display','none')
           }
        $('.getOtherTlv').on('change', function() {
           if(this.value == 'Other'){
               $('#otherTlv').css('display','block')
           }
        });

        if(document.getElementById('degree').value == ""){
            $('#blankDiv').css('display','block')
        }
        if(document.getElementById('degree').value == "Others"){
            $('#dept_branch_subjectDiv').css('display','block')
            $('#specializationDiv').css('display','none')
            $('#blankDiv').css('display','none')
        }
        $('#degree').on('change', function() {
           if(this.value == 'Others'){
               $('#dept_branch_subjectDiv').css('display','block')
               $('#specializationDiv').css('display','none')
               $('#blankDiv').css('display','none')
           }
        });

        if(document.getElementById('degree').value != "Others" && document.getElementById('degree').value != ""){
            $('#dept_branch_subjectDiv').css('display','none')
            $('#specializationDiv').css('display','block')
            $('#blankDiv').css('display','none')
        }
        $('#degree').on('change', function() {
           if(this.value != 'Others' && document.getElementById('degree').value != ""){
               $('#dept_branch_subjectDiv').css('display','none')
               $('#specializationDiv').css('display','block')
               $('#blankDiv').css('display','none')
           }
        });

        $('#degree').on('change', function() {
           if(document.getElementById('degree').value == ""){
               $('#dept_branch_subjectDiv').css('display','none')
               $('#specializationDiv').css('display','none')
               $('#blankDiv').css('display','block')
           }
        });

        $('.lengthC').keypress(function(e) {
            var foo = $(this).val()
            if (foo.length >= 10) { //specify text limit
                return false;
            }
            return true;
        });
        $('#state').change(function(e){
            var city = e.target.value;
            var cityList = state[city]
            cc = '<option value="">--select--</option>'
            cityList.forEach(element => {
                cc +='<option value="'+element+'">'+element+'</option>'
            });
            $('#city').html(cc);
        })

        $('#program').change(function(e){
            var program = e.target.value;
            var programList = programs[program]
            pp = '<option value="">--select--</option>'
            programList.forEach(element => {
                pp +='<option value="'+element+'">'+element+'</option>'
            });
            $('#specific_programme').html(pp);
        })
    }); 
    
    $(document).ready(function() {
    
    
    $("#photo").change(function() {
     $("#photoMsg").html("");
        var file_size = $('#photo')[0].files[0].size;
        var file_type = $('#photo')[0].files[0].type;
        if (file_size > 2000000) {
            $("#photoMsg").html("File size could not be more than 2 mb");
           
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#photoMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#photo').val('')
            return false;
        }
        return true;
    })
    
     $("#transfer_certificate").change(function() {
        $("#transfer_certificateMsg").html("");
        var file_size = $('#transfer_certificate')[0].files[0].size;
        var file_type = $('#transfer_certificate')[0].files[0].type;
        if (file_size > 2000000) {
            $("#transfer_certificateMsg").html("File size could not be more than 2 mb");
           
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#transfer_certificateMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#transfer_certificate').val('')
            return false;
        }
        return true;
    })
    
    
     $("#conduct_certificate").change(function() {
        $("#conduct_certificateMsg").html("");
        var file_size = $('#conduct_certificate')[0].files[0].size;
        var file_type = $('#conduct_certificate')[0].files[0].type;
        if (file_size > 2000000) {
            $("#conduct_certificateMsg").html("File size could not be more than 2 mb");
           
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#conduct_certificateMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#transfer_certificate').val('')
            return false;
        }
        return true;
    })
    
    
    
    $("#mark_sheet").change(function() {
        $("#mark_sheetMsg").html("");
        var file_size = $('#mark_sheet')[0].files[0].size;
        var file_type = $('#mark_sheet')[0].files[0].type;
        if (file_size > 2000000) {
            $("#mark_sheetMsg").html("File size could not be more than 2 mb");
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#mark_sheetMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#mark_sheet').val('')
            return false;
        }
        return true;
    })
    
    
        $("#provisional_degree_certificate").change(function() {
         $("#provisional_degree_certificateMsg").html("");
        var file_size = $('#provisional_degree_certificate')[0].files[0].size;
        var file_type = $('#provisional_degree_certificate')[0].files[0].type;
        if (file_size > 2000000) {
            $("#provisional_degree_certificateMsg").html("File size could not be more than 2 mb");
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#provisional_degree_certificateMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#provisional_degree_certificate').val('')
            return false;
        }
        return true;
    })
    
    
    $("#aadhar_card_front").change(function() {
    $("#aadhar_card_frontMsg").html("");
        var file_size = $('#aadhar_card_front')[0].files[0].size;
        var file_type = $('#aadhar_card_front')[0].files[0].type;
        if (file_size > 2000000) {
            $("#aadhar_card_frontMsg").html("File size could not be more than 2 mb");
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#aadhar_card_frontMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#aadhar_card_front').val('')
            return false;
        }
        return true;
    })
    
    
    $("#aadhar_card_back").change(function() {
        $("#aadhar_card_backMsg").html("");
        var file_size = $('#aadhar_card_back')[0].files[0].size;
        var file_type = $('#aadhar_card_back')[0].files[0].type;
        if (file_size > 2000000) {
            $("#aadhar_card_backMsg").html("File size could not be more than 2 mb");
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#aadhar_card_backMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#aadhar_card_back').val('')
            return false;
        }
        return true;
    })
    
    $("#aadhar_card_front").change(function() {
         $("#aadhar_card_frontMsg").html("");
        var file_size = $('#aadhar_card_front')[0].files[0].size;
        var file_type = $('#aadhar_card_front')[0].files[0].type;
        if (file_size > 2000000) {
            $("#aadhar_card_frontMsg").html("File size could not be more than 2 mb");
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#aadhar_card_frontMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#aadhar_card_front').val('')
            return false;
        }
        return true;
    })
    
    
    $("#parent_aadhar_card_back").change(function() {
        $("#parent_aadhar_card_backMsg").html("");
        var file_size = $('#parent_aadhar_card_back')[0].files[0].size;
        var file_type = $('#parent_aadhar_card_back')[0].files[0].type;
        if (file_size > 2000000) {
            $("#parent_aadhar_card_backMsg").html("File size could not be more than 2 mb");
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#parent_aadhar_card_backMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#parent_aadhar_card_back').val('')
            return false;
        }
        return true;
    })
    
    $("#parent_aadhar_card_front").change(function() {
         $("#parent_aadhar_card_frontMsg").html("");
        var file_size = $('#parent_aadhar_card_front')[0].files[0].size;
        var file_type = $('#parent_aadhar_card_front')[0].files[0].type;
        if (file_size > 2000000) {
            $("#parent_aadhar_card_frontMsg").html("File size could not be more than 2 mb");
            if (['image/jpeg', 'image/png','application/pdf'].indexOf(file_type) < 0) {
                $("#parent_aadhar_card_frontMsg").html("Please upload jpg, jpeg, png or pdf file");
            }
            $('#parent_aadhar_card_front').val('')
            return false;
        }
        return true;
    })
    
    
    
    
    
    
})
</script>
</html>