<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="icon" type="image/x-icon" href="{{ url('/assets/img/logo.png') }}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>{{ config('app.name', 'Laravel') }}</title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
		<!--     Fonts and icons     -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
		<!-- CSS Files -->
		<!-- date picker css -->
		<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
		<link href="{{url('/public/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
		<link href="{{ url('/public/assets/css/light-bootstrap-dashboard.css?v=2.0.0')}} " rel="stylesheet" />
		<!-- CSS Just for demo purpose, don't include it in your project -->
		<link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" />
		<link href="{{url('/public/assets/css/demo.css')}}" rel="stylesheet" />
		
	</head>
	<body>
		<div class="wrapper">
			<div class="sidebar" data-image="{{ ('/public/assets/img/sidebar-5.jpg') }}">
				<!--
					Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"
					
					Tip 2: you can also add an image using data-image tag
					-->
				<div class="sidebar-wrapper">
					<div class="logo">
						<a href="#" class="simple-text">
						Online Application
						</a>
					</div>
					<ul class="nav">
						<li>
							<a class="nav-link" href="{{ url('applications')}}">
							
								<p>Applications</p>
							</a>
								<ul class="sub-item">
								
									<li>
										<a class="nav-link" href="{{ url('applications/all?s=Pending')}}">
											<p>Pending</p>
										</a>
										
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?s=Accepted')}}">
											<p>Accepted</p>
										</a>
										
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?s=Rejected')}}">
											<p>Rejected</p>
										</a>
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?s=Fee+pending')}}">
											<p>Fee pending (form 2)</p>
										</a>
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?s=Fee+paid')}}">
											<p>Fee Paid (form 2)</p>
										</a>
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?p=M.sc.')}}">
											<p>M.sc</p>
										</a>
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?p=M.C.A')}}">
											<p>M.C.A</p>
										</a>
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?p=M.Com')}}">
											<p>M.Com</p>
										</a>
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?p=M.B.A')}}">
											<p>M.B.A</p>
										</a>
									</li>
									<li>
										<a class="nav-link" href="{{ url('applications/all?p=M.A.')}}">
											<p>M.A</p>
										</a>
									</li>
								</ul>
							</li>
					</ul>
				</div>
			</div>
			<div class="main-panel">
				
                <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#pablo">
					<img src="{{ url('/public/assets/img/logo.png') }}" style="height: 40px"/>
					</a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="dropdown">
                                    
                                    <span class="d-lg-none">Dashboard</span>
                                </a>
                            </li>
                            
                           
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/logout')}}">
                                    <span class="no-icon">Log out</span>
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </nav>
				<!-- End Navbar -->
				<div class="content">
					<div class="container-fluid">
						<div class="row justify-content-center">
							<div class="col-md-10">
								@if(Session::has('message'))
									<p class="alert alert-info">{{ Session::get('message') }}</p>
								@endif
								@if(Session::has('success'))
									<p class="alert alert-success">{{ Session::get('success') }}</p>
								@endif
								@if(Session::has('warning'))
									<p class="alert alert-warning">{{ Session::get('warning') }}</p>
								@endif
							</div>
						</div>

						<div class="row">
							@yield('content')
						</div>
					</div>
				</div>
				<footer class="footer">
					<div class="container-fluid">
						<nav>
							
							<p class="copyright text-center">
							<a href="http://kathir.biz/" target="_blank">Kathir Technologies</a> © 

								<script>
									document.write(new Date().getFullYear())
								</script>
								 made with love for a better web
							</p>
						</nav>
					</div>
				</footer>
			</div>
		</div>
		
	</body>
	<!--   Core JS Files   -->
	<script src="/public/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="/public/assets/js/core/popper.min.js" type="text/javascript"></script>
	<script src="/public/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
	<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
	<script src="/public/assets/js/plugins/bootstrap-switch.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<!--  Google Maps Plugin    -->
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
	<!--  Chartist Plugin  -->
	<!-- date picker js -->
	<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
	<script src="{{ ('/public/assets/js/plugins/chartist.min.js') }}"></script>
	<!--  Notifications Plugin    -->
	<script src="{{ ('/public/assets/js/plugins/bootstrap-notify.js') }}"></script>
	<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
	<script src="{{ ('/public/assets/js/light-bootstrap-dashboard.js?v=2.0.0') }}" type="text/javascript"></script>
	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<script src="{{('/public/assets/js/demo.js') }}"></script>
	<script src="{{ ('/public/assets/js/city.js') }}"></script>
	<script>
		$(function () {
    	var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url('applications') }}",
        columns: [
            {data: 'name', name: 'name', searchable: true},
			{data: 'mobile_number', name: 'mobile_number'},
			{data: 'email_id', name: 'email_id'},
			{data: 'program', name: 'program'},
			{data: 'specific_programme', name: 'specific_programme'},
			{data: 'razorpay_payment_id', name: 'razorpay_payment_id'},
			{data: 'application_status', name: 'application_status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
		],
		// initComplete: function () {
        //     this.api().columns().every(function () {
		// 		var column = this;
        //         var input = document.createElement("input");
        //         $(input).appendTo($(column.header()))
        //         .on('keyup', function () {
        //             var val = $.fn.dataTable.util.escapeRegex($(this).val());
        //             column.search(val ? val : '', true, false).draw();
        //         });
        //     });
        // }
    });

	$('#program').change(function(e){
		var program = e.target.value;
		var programList = programs[program]
		pp = '<option value="">--select--</option>'
		programList.forEach(element => {
			pp +='<option value="'+element+'">'+element+'</option>'
		});
		$('#specific_programme').html(pp);
	})
    
  });
		    
	</script>
</html>